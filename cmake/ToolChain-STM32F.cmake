# Toolchain file for the STM32F target platform
#
# This may be used in contributed code for that platform.
#
# Inspired on various documents, and a pile of examples on
# https://github.com/dwelch67/stm32_samples/blob/master/STM32F103C8T6/
#
# From: Rick van Rein <rick@openfortress.nl>


set (_PLATFORM arm-none-eabi-)

# the name of the target operating system
set (CMAKE_SYSTEM_NAME FreeRTOS)

# which compilers to use for C and C++
set (CMAKE_C_COMPILER   ${_PLATFORM}gcc)
set (CMAKE_CXX_COMPILER ${_PLATFORM}g++)
add_compile_options (-nostartfiles)
# add_compile_options (-lc)
add_compile_options (-specs=nosys.specs -nostdlib -nostartfiles -ffreestanding -O2 -mthumb -mcpu=cortex-m3 -march=armv7-m)
add_link_options (-specs=nosys.specs -nostdlib -nostartfiles -ffreestanding)

# where is the target environment located
# set(CMAKE_FIND_ROOT_PATH /usr/bin)

# adjust the default behavior of the FIND_XXX() commands:
# search programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# search headers and libraries in the target environment
# set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
# set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# Scan for the chip if we don't know its size yet
#
if ("${STM32F_SRAM}" STREQUAL "")
	execute_process (
		COMMAND st-info --sram
		OUTPUT_VARIABLE _PROBED_VALUE
		ERROR_QUIET
		RESULT_VARIABLE _PROBE_EXIT)
	if (_PROBE_EXIT EQUAL 0)
		string (STRIP "${_PROBED_VALUE}" _PROBED_VALUE)
		set (STM32F_SRAM ${_PROBED_VALUE}
			CACHE STRING "Bytes of SRAM in this STM32F model")
		message (STATUS "Detected SRAM  size ${STM32F_SRAM}")
	endif ()
endif ()
if ("${STM32F_FLASH}" STREQUAL "")
	execute_process (
		COMMAND st-info --flash
		OUTPUT_VARIABLE _PROBED_VALUE
		ERROR_QUIET
		RESULT_VARIABLE _PROBE_EXIT)
	if (_PROBE_EXIT EQUAL 0)
		string (STRIP "${_PROBED_VALUE}" _PROBED_VALUE)
		set (STM32F_FLASH ${_PROBED_VALUE}
			CACHE STRING "Bytes of Flash in this STM32F model")
		message (STATUS "Detected FLASH size ${STM32F_FLASH}")
	endif ()
endif ()

# Produce an adapted STM32F linker/loader file
#
configure_file (
	${CMAKE_SOURCE_DIR}/cmake/tools/flash-STM32F.ld.in
	${CMAKE_BINARY_DIR}/flash-STM32F.ld
	@ONLY)
add_link_options (-T ${CMAKE_BINARY_DIR}/flash-STM32F.ld)

# We construct platform-suitable firmware (maybe multiple versions) with
# add_firmware (some.bin some.elf)
#
function (add_firmware _OUTPUT _INPUT)
	add_custom_target ("${_OUTPUT}" ALL)
	add_custom_command (
		TARGET "${_OUTPUT}"
		COMMAND ${_PLATFORM}objcopy "${_INPUT}" "${_OUTPUT}" -O binary
		DEPENDS "${_INPUT}"
		COMMENT "Extracting firmware image ${_OUTPUT}"
	)
endfunction ()

# We construct assembler listings with
# add_listing (some.lst some.elf)
#
function (add_listing _OUTPUT _INPUT)
	add_custom_target ("${_OUTPUT}" ALL)
	add_custom_command (
		TARGET "${_OUTPUT}"
		COMMAND ${_PLATFORM}objdump -D "${_INPUT}" > "${_OUTPUT}"
		DEPENDS "${_INPUT}"
		COMMENT "Disassembling listing ${_OUTPUT}"
	)
endfunction ()
