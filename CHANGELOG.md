# CHANGELOG for controller-p1port-gy39



## Earlier work on the code

This work started in the mbusc project, but grew into a separate project.
The development history has been left on a branch but continues here.

History preceding the work here can be found on
https://gitlab.com/groengemak/mbusc/-/tree/contrib-p1port-gy39?ref_type=heads

The commit that was deported is
a336f5521a338b34cb16784b5a11ca30d3ff2ba7
with files browseable at
https://gitlab.com/groengemak/mbusc/-/tree/a336f5521a338b34cb16784b5a11ca30d3ff2ba7

The deportation commit following this is
2873ec90552b6872a6a9895317f5711ee563e134
found at
https://gitlab.com/groengemak/mbusc/-/commit/2873ec90552b6872a6a9895317f5711ee563e134
