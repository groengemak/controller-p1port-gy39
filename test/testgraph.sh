#!/bin/sh
#
# Draw a graph over the last day, args [[RRDfile] PNGfile]
#
# From: Rick van Rein <rick@openfortress.nl>


if [ $# -eq 0 ]
then
	RRDFILE="/tmp/test.rrd"
	PNGFILE="/tmp/test.png"
elif [ $# -eq 1 ]
then
	RRDFILE="/tmp/test.rrd"
	PNGFILE="$1"
elif [ $# -eq 2 ]
then
	RRDFILE="$1"
	PNGFILE="$2"
else
	echo >&1 "Usage: $0 [[file.rrd] file.png]"
	exit 1
fi

rrdtool graph "$PNGFILE.1d.temp"  --end now --start now-86400s --width 800 --height=600 \
	"DEF:t=$RRDFILE:temp:AVERAGE"  'LINE1:t#0000ff:Temperature [⁹C]\l'
rrdtool graph "$PNGFILE.1d.humid" --end now --start now-86400s --width 800 --height=600 \
	"DEF:h=$RRDFILE:humid:AVERAGE" 'LINE1:h#00ff00:Humidity [%]\l'

rrdtool graph "$PNGFILE.1w.temp"  --end now --start now-604800s --width 800 --height=600 \
	"DEF:t=$RRDFILE:temp:AVERAGE"  'LINE1:t#0000ff:Temperature [⁹C]\l'
rrdtool graph "$PNGFILE.1w.humid" --end now --start now-604800s --width 800 --height=600 \
	"DEF:h=$RRDFILE:humid:AVERAGE" 'LINE1:h#00ff00:Humidity [%]\l'

rrdtool graph "$PNGFILE.1m.temp"  --end now --start now-2678400s --width 800 --height=600 \
	"DEF:t=$RRDFILE:temp:AVERAGE"  'LINE1:t#0000ff:Temperature [⁹C]\l'
rrdtool graph "$PNGFILE.1m.humid" --end now --start now-2678400s --width 800 --height=600 \
	"DEF:h=$RRDFILE:humid:AVERAGE" 'LINE1:h#00ff00:Humidity [%]\l'

