#!/bin/sh
#
# Create a test RRD in the named file (default /tmp/test.rrd).
#
# The RRD holds fields "temp" and "humid" in 1-second samples and
# summarises them over a minute, hour and day.
#
# From: Rick van Rein <rick@openfortress.nl>


FILENAME="${1:-/tmp/test.rrd}"

if [ -f "$FILENAME" ]
then
	echo >&1 'Already have that file.  Refusing.'
	exit 1
fi

rrdcreate "$FILENAME" --step 60 \
		DS:temp:GAUGE:60:-50:100 \
		DS:humid:GAUGE:60:0:100 \
		RRA:AVERAGE:0.5:1:1440 \
		RRA:AVERAGE:0.5:60:168 \
		RRA:AVERAGE:0.5:1440:62 \

rrdinfo "$FILENAME"

