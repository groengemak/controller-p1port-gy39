/** @defgroup arpa2comerr Common Error handling, portable include
 * @{
 * 
 * Most ARPA2 systems report errors in `errno`, but without being too
 * agressive; the Common Error system is an old, but still the best
 * idea of compressing a table name into 3 bytes to address a table
 * of 256 contiguous error codes.  The risk of clashes when packages
 * are arbitrarily composed is minimal.
 * 
 * The following is an exmple of how ARPA2 Identity reports an error:
 * 
 * ```
 * #include <com_err/arpa2identity.h>
 * 
 * errno = A2ID_ERR_SELECTOR_SYNTAX;
 * return false;
 * ```
 * 
 * The low risk of clashes means we can use `errno`, which is already
 * supported broadly.  The value may travel up through several layers,
 * but because `a2id_init()` registered the error messages in a
 * global table, these higher-up layers can produce meaningful errors
 * without knowing about a low-down layer like ARPA2 Identity, so
 * 
 * 
 * ```
 * #include <arpa2/com_err.h>
 * 
 * com_err ("newpkg", errno, "- Failed to have fun with %d", 2);
 * ```
 * 
 * would print something meaningful like
 * 
 * ```
 * newpkg: ARPA2 Selector syntax error: Failed to have fun with 2
 * ```
 * 
 * We actually use it with a wrapper that auto-detects Common Errors,
 * 
 * ```
 * #include <arpa2/com_err.h>
 * #include <arpa2/except.h>
 * 
 * log_errno ("Signature verification failure in a2id_verify()");
 * ```
 * 
 * (basically replacing `log_error()` with `log_errno()` and no further changes)
 * to yield the much more informative message
 * 
 * ```
 * Error: ARPA2 Identity expired: Signature verification failure in a2id_verify()
 * ```
 * 
 * where the expiration of the Identity is a helpful hint, shared in
 * `errno` with the declaration in `lib/id/errno.et` as
 * 
 * ```
 * error_table A2ID
 * ...
 * error_code A2ID_ERR_EXPIRED,
 *         "ARPA2 Identity expired"
 * ...
 * ```
 * 
 * Check the file to learn more about the errors you might expect.
 * It will show you how much information we would loose by constraining
 * our errors to POSIX error codes or just a boolean.
 *
 * The include file <arpa2/com_err.h>  handles variations in the location
 * of the "real" <com_err.h>, which:
 *
 *   - Linux lives in a separate package (filesystem-utilities) and ends up as
 *     </usr/include/et/com_err.h>
 *   - FreeBSD has it in base, without subdirectory, so it ends up as
 *     </usr/include/com_err.h> (This might be different with FreeBSD 12)
 *
 */

/* This file was part of ARPA2CM, version 0.8.3 and later.
 *     https://gitlab.com/arpa2/arpa2cm
 *
 * It was later included into the ARPA2 Common Libraries, fetching from
 * commit 6c995b1a28190c1c5f477edca642999ebf45e243 in ARPA2CM.
 *
 * Redistribution and use is allowed according to the terms of the two-clause BSD license.
 *    SPDX-License-Identifier: BSD-2-Clause
 *    SPDX-FileCopyrightText: Copyright 2020 Adriaan de Groot <groot@kde.org>
 *
 */

#ifndef ARPA2_COM_ERR_H
#define ARPA2_COM_ERR_H


/** @brief Initialise the COM-ERR list of error codes.
 *
 * This call is not usually needed, but a COM-ERR library may count on
 * the list being set to NULL before getting started and this can fail
 * if a program loads a module as a shared object that in turn loads
 * the COM-ERR library.  In such cases, you may need to explicitly
 * initialise.
 *
 * If you program uses threads, then use pthread_once() or similar to
 * avoid calling it concurrently, using
 *
 *    static pthread_once_t a2ce_ctl = PTHREAD_ONCE_INIT;
 *    pthread_once (*a2ce_ctl, arpa2comerr_init);
 *
 * It is not a problem if multiple modules call this function in
 * sequence, as long as these code fragments are not concurrently
 * called, which is not usually what modular programs do.
 */
void arpa2comerr_init (void);

#ifdef __FreeBSD__
#include <com_err.h>
#else
#include <et/com_err.h>
#endif

#endif

/** @} */
