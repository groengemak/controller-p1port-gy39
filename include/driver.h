/* driver.h -- Hardware API for microcontrollers.
 *
 * These routines will be assumed in any microcontroller, in some form.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdbool.h>
#include <stdint.h>



/* Simple NOP call, to bypass optimising out active waiting loop code.
 */
void nopCall (void);


/* Set the on-board LED on or off via bit-banding.
 */
void boardLED (bool light);


/* Set the output signal DREQ, indicating whether the P1 Port is active.
 */
void setDREQ (bool active);


/* Select or deselect one of the independent SELn signals.
 * Any GY-39 fed from the SELn line is powered up or down.
 * Glitches due to switching to the same are avoided.
 *
 * Multiple GY-39 sharing a line would clash, so a bus must be
 * constructed for such situations.  At most one even SELn and
 * one odd SELn will be activated by this routine at any time.
 *
 * Setting and clearing a line are idempotent operations.
 *
 * This has no impact on remapping the USART for GY-39 sensors.
 */
void setSELn (unsigned n, bool active);


/* Remap USART1 to the desired GY-39 sensor bus (posA or posB).
 *
 * This has no bearing on the SELn signals, use setSELn() for that.
 *
 * One possible design connects the TX's from both positions and
 * RX's from both positions.  We shall work to avoid short-circuit
 * by switching the losing TX to a pull-up input before switching
 * the gaining TX to alternate push-pull output.
 */
void selGY39 (unsigned n);


/* Sample the current value on CONS.
 */
bool getCONS (void);


/* See if CONS saw a rising edge since the last time we checked.
 *
 * Avoid missing signals; detect the event and only then clear it.
 * Multiple events occurring since the last check are combined into one.
 * 
 */
bool risenCONS (void);


/* Start sending to GY-39 on USART1.
 */
void start_send_GY39 (void);

/* Stop sending to GY-39 on USART1.
 */
void stop_send_GY39 (void);

/* Send a character to GY-39 on USART1.
 */
void send_GY39 (uint8_t ch);

/* Start receiving from GY-39 over USART1.
 */
void start_recv_GY39 (void);

/* Stop receiving from GY-39 over USART1.
 */
void stop_recv_GY39 (void);

/* Receive a character from GY-39 over USART1.  Return success.
 */
bool recv_GY39 (uint8_t *ch);


/* Start sending to CONS on USART2.
 */
void start_send_CONS (void);

/* Stop sending to CONS on USART2.
 */
void stop_send_CONS (void);

/* Send a character string to CONS on USART2.  The output is
 * sent via DMA1 controller, channel 7, with aptly low priority.
 *
 * The proper thing to do is to wait for the interrupt before
 * sending another transmission (a new one would otherwise break
 * off an ongoing transmission, which makes for ugly reading).
 */
void send_CONS (const char *txt, uint16_t txtlen);

/* Start receiving from the P1 Port over USART2.
 */
void start_recv_P1 (void);

/* Stop receiving from the P1 Port over USART2.
 */
void stop_recv_P1 (void);

/* Receive a character from the P1 Port over USART2.  Return success.
 */
bool recv_P1 (uint8_t *ch);


/* Start sending to Modbus on USART3.
 */
void start_send_Modbus (void);

/* Stop sending to Modbus on USART3.
 */
void stop_send_Modbus (void);

/* Send a character to Modbus on USART3.
 */
void send_Modbus (uint8_t ch);

/* Start receiving from Modbus over USART3.
 */
void start_recv_Modbus (void);

/* Stop receiving from Modbus over USART3.
 */
void stop_recv_Modbus (void);

/* Receive a character from Modbus over USART3.  Return success.
 */
bool recv_Modbus (uint8_t *ch);


/* Send data out over USART1, USART2 or USART3.
 */
void serialTX (uint8_t *dev, uint8_t *msg, unsigned msglen);


/* Poll to read a data byte from USART1, USART2 or USART3.
 *
 * When a  byte is pending, store it in *data and return true .
 * When no byte is pending, don't alter *date and return false.
 */
bool serialRXpoll (uint8_t *dev, uint8_t *data);


/* General hardware setup.
 */
void setupDriver (void);


/* Substitute assert() will lock the CPU into an infinite loop upon error.
 * This should be helpful with an interactive debugger attached.  That's
 * why this is only active with DEBUG configured.
 */
#if DEBUG
#define assert(cc) do { volatile int line = __LINE__; } while (!(cc))
#else
#define assert(cc)
#endif
