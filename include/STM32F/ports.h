/* Ports in the STM32F architecture.
 *
 * TODO: Maybe this is specific for a more detailed model,
 * such as STM32F103 or even STM32F103C8T6?
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdint.h>
#include <stdbool.h>



/* Introduce a barrier between dependent register configurations.
 */
#define REGISTER_BARRIER() asm volatile("": : :"memory")


/* Base addresses for 32-bit peripherals.
 */
#define NVIC	((uint8_t*) 0xe000e000)
#define RCC	((uint8_t*) 0x40021000)
#define I2C1	((uint8_t*) 0x40005400)
#define I2C2	((uint8_t*) 0x40005800)
#define AFIO	((uint8_t*) 0x40010000)
#define EXTI	((uint8_t*) 0x40010400)
#define GPIOA	((uint8_t*) 0x40010800)
#define GPIOB	((uint8_t*) 0x40010c00)
#define GPIOC	((uint8_t*) 0x40011000)
#define USART1	((uint8_t*) 0x40013800)
#define USART2	((uint8_t*) 0x40004400)
#define USART3	((uint8_t*) 0x40004800)
#define DMA1	((uint8_t*) 0x40020000)
#define DMA2	((uint8_t*) 0x40020400)

#define NVIC_ISER	(*(uint32_t*) (NVIC + 0x100))
#define NVIC_ICER	(*(uint32_t*) (NVIC + 0x180))
#define NVIC_ISPR	(*(uint32_t*) (NVIC + 0x200))
#define NVIC_ICPR	(*(uint32_t*) (NVIC + 0x280))

#define RCC_CR		(*(uint32_t*) (RCC + 0x00))
#define RCC_CFGR	(*(uint32_t*) (RCC + 0x04))
#define RCC_CIR		(*(uint32_t*) (RCC + 0x08))
#define RCC_APB2RSTR	(*(uint32_t*) (RCC + 0x0c))
#define RCC_APB1RSTR	(*(uint32_t*) (RCC + 0x10))
#define RCC_AHBENR	(*(uint32_t*) (RCC + 0x14))
#define RCC_APB2ENR	(*(uint32_t*) (RCC + 0x18))
#define RCC_APB1ENR	(*(uint32_t*) (RCC + 0x1c))
#define RCC_BDCR	(*(uint32_t*) (RCC + 0x20))

#define AFIO_EVCR	(*(uint32_t*) (AFIO + 0x00))
#define AFIO_MAPR	(*(uint32_t*) (AFIO + 0x04))
#define AFIO_EXTICR1	(*(uint32_t*) (AFIO + 0x08))
#define AFIO_EXTICR2	(*(uint32_t*) (AFIO + 0x0c))
#define AFIO_EXTICR3	(*(uint32_t*) (AFIO + 0x10))
#define AFIO_EXTICR4	(*(uint32_t*) (AFIO + 0x14))
#define AFIO_MAPR2	(*(uint32_t*) (AFIO + 0x1c))

#define EXTI_IMR	(*(uint32_t*) (EXTI + 0x00))
#define EXTI_EMR	(*(uint32_t*) (EXTI + 0x04))
#define EXTI_RTSR	(*(uint32_t*) (EXTI + 0x08))
#define EXTI_FTSR	(*(uint32_t*) (EXTI + 0x0c))
#define EXTI_SWIER	(*(uint32_t*) (EXTI + 0x10))
#define EXTI_PR		(*(uint32_t*) (EXTI + 0x14))

#define GPIOA_CRL	(*(uint32_t*) (GPIOA + 0x00))
#define GPIOA_CRH	(*(uint32_t*) (GPIOA + 0x04))
#define GPIOA_IDR	(*(uint32_t*) (GPIOA + 0x08))
#define GPIOA_ODR	(*(uint32_t*) (GPIOA + 0x0c))
#define GPIOA_BSRR	(*(uint32_t*) (GPIOA + 0x10))
#define GPIOA_BRR	(*(uint32_t*) (GPIOA + 0x14))

#define GPIOB_CRL	(*(uint32_t*) (GPIOB + 0x00))
#define GPIOB_CRH	(*(uint32_t*) (GPIOB + 0x04))
#define GPIOB_IDR	(*(uint32_t*) (GPIOB + 0x08))
#define GPIOB_ODR	(*(uint32_t*) (GPIOB + 0x0c))
#define GPIOB_BSRR	(*(uint32_t*) (GPIOB + 0x10))
#define GPIOB_BRR	(*(uint32_t*) (GPIOB + 0x14))

#define GPIOC_CRL	(*(uint32_t*) (GPIOC + 0x00))
#define GPIOC_CRH	(*(uint32_t*) (GPIOC + 0x04))
#define GPIOC_IDR	(*(uint32_t*) (GPIOC + 0x08))
#define GPIOC_ODR	(*(uint32_t*) (GPIOC + 0x0c))
#define GPIOC_BSRR	(*(uint32_t*) (GPIOC + 0x10))
#define GPIOC_BRR	(*(uint32_t*) (GPIOC + 0x14))

#define USART1_SR	(*(uint16_t*) (USART1 + 0x00))
#define USART1_DR	(*(uint16_t*) (USART1 + 0x04))
#define USART1_BRR	(*(uint16_t*) (USART1 + 0x08))
#define USART1_CR1	(*(uint16_t*) (USART1 + 0x0c))
#define USART1_CR2	(*(uint16_t*) (USART1 + 0x10))
#define USART1_CR3	(*(uint16_t*) (USART1 + 0x14))
#define USART1_GPTR	(*(uint16_t*) (USART1 + 0x18))

#define USART2_SR	(*(uint16_t*) (USART2 + 0x00))
#define USART2_DR	(*(uint16_t*) (USART2 + 0x04))
#define USART2_BRR	(*(uint16_t*) (USART2 + 0x08))
#define USART2_CR1	(*(uint16_t*) (USART2 + 0x0c))
#define USART2_CR2	(*(uint16_t*) (USART2 + 0x10))
#define USART2_CR3	(*(uint16_t*) (USART2 + 0x14))
#define USART2_GPTR	(*(uint16_t*) (USART2 + 0x18))

#define USART3_SR	(*(uint16_t*) (USART3 + 0x00))
#define USART3_DR	(*(uint16_t*) (USART3 + 0x04))
#define USART3_BRR	(*(uint16_t*) (USART3 + 0x08))
#define USART3_CR1	(*(uint16_t*) (USART3 + 0x0c))
#define USART3_CR2	(*(uint16_t*) (USART3 + 0x10))
#define USART3_CR3	(*(uint16_t*) (USART3 + 0x14))
#define USART3_GPTR	(*(uint16_t*) (USART3 + 0x18))

#define DMA1_ISR	(*(uint32_t*) (DMA1 + 0x00))
#define DMA1_IFCR	(*(uint32_t*) (DMA1 + 0x04))
#define DMA1_CCR1	(*(uint32_t*) (DMA1 + 0x08 -20 + 20*1))
#define DMA1_CNDTR1	(*(uint32_t*) (DMA1 + 0x0c -20 + 20*1))
#define DMA1_CPAR1	(*(uint32_t*) (DMA1 + 0x10 -20 + 20*1))
#define DMA1_CMAR1	(*(uint32_t*) (DMA1 + 0x14 -20 + 20*1))
#define DMA1_CCR2	(*(uint32_t*) (DMA1 + 0x08 -20 + 20*2))
#define DMA1_CNDTR2	(*(uint32_t*) (DMA1 + 0x0c -20 + 20*2))
#define DMA1_CPAR2	(*(uint32_t*) (DMA1 + 0x10 -20 + 20*2))
#define DMA1_CMAR2	(*(uint32_t*) (DMA1 + 0x14 -20 + 20*2))
#define DMA1_CCR3	(*(uint32_t*) (DMA1 + 0x08 -20 + 20*3))
#define DMA1_CNDTR3	(*(uint32_t*) (DMA1 + 0x0c -20 + 20*3))
#define DMA1_CPAR3	(*(uint32_t*) (DMA1 + 0x10 -20 + 20*3))
#define DMA1_CMAR3	(*(uint32_t*) (DMA1 + 0x14 -20 + 20*3))
#define DMA1_CCR4	(*(uint32_t*) (DMA1 + 0x08 -20 + 20*4))
#define DMA1_CNDTR4	(*(uint32_t*) (DMA1 + 0x0c -20 + 20*4))
#define DMA1_CPAR4	(*(uint32_t*) (DMA1 + 0x10 -20 + 20*4))
#define DMA1_CMAR4	(*(uint32_t*) (DMA1 + 0x14 -20 + 20*4))
#define DMA1_CCR5	(*(uint32_t*) (DMA1 + 0x08 -20 + 20*5))
#define DMA1_CNDTR5	(*(uint32_t*) (DMA1 + 0x0c -20 + 20*5))
#define DMA1_CPAR5	(*(uint32_t*) (DMA1 + 0x10 -20 + 20*5))
#define DMA1_CMAR5	(*(uint32_t*) (DMA1 + 0x14 -20 + 20*5))
#define DMA1_CCR6	(*(uint32_t*) (DMA1 + 0x08 -20 + 20*6))
#define DMA1_CNDTR6	(*(uint32_t*) (DMA1 + 0x0c -20 + 20*6))
#define DMA1_CPAR6	(*(uint32_t*) (DMA1 + 0x10 -20 + 20*6))
#define DMA1_CMAR6	(*(uint32_t*) (DMA1 + 0x14 -20 + 20*6))
#define DMA1_CCR7	(*(uint32_t*) (DMA1 + 0x08 -20 + 20*7))
#define DMA1_CNDTR7	(*(uint32_t*) (DMA1 + 0x0c -20 + 20*7))
#define DMA1_CPAR7	(*(uint32_t*) (DMA1 + 0x10 -20 + 20*7))
#define DMA1_CMAR7	(*(uint32_t*) (DMA1 + 0x14 -20 + 20*7))

/* Direct bit access in SRAM and registers via Cortex-M3 bit-banding.
 * 
 * Read/write bit-band access to any register via BIT(portreg,bitnr).
 * This can be used with bool values (when they are 0 or 1).
 * In general however, BIT(portreg,bitnr) is typed as (uint8_t*).
 * Call it with the register's address in portreg and bitnr 0..31.
 *
 * The address calculation is addrsz-integer math, with masks over
 * bitfields to avoid access outside of the 32 MB bit-banding areas,
 * but there is no check whether the address is SRAM or a register.
 * This calculation is completely constant when reg and bitnr are.
 *
 * The portreg is ANDed with 0x000fffff to derive its 1 MB range.
 * The bit-band base is (register AND 0x60000000) OR 0x02000000.
 * OR this with the 1-MB-ranged portreg times 32 to find the word.
 * OR this with the bitnr times 4 to find the bit-banding address.
 */
#define BIT(reg,bitnr) ( \
				* (uint32_t*) ( \
					((((intptr_t) &(reg)) & 0x60000000)     ) | \
					0x02000000 | \
					((((intptr_t) &(reg)) & 0x000fffff) << 5) | \
					(((bitnr) & 0x1f) << 2) \
				) \
                       )
#define BIT0(reg,bitnr) ( \
				* (uint32_t*) ( \
					((((intptr_t) &(reg)) & 0x60000000)     ) | \
					0 \
				) \
                       )
#define BIT1(reg,bitnr) ( \
				* (uint32_t*) ( \
					0x02000000 | \
					0 \
				) \
                       )
#define BIT2(reg,bitnr) ( \
				* (uint32_t*) ( \
					((((intptr_t) &(reg)) & 0x000ffffc) << 5) | \
					0 \
				) \
                       )
#define BIT3(reg,bitnr) ( \
				* (uint32_t*) ( \
					(intptr_t) (((bitnr) & 0x1f) << 2) \
				) \
                       )


/* In case of doubt, translate a bool into either 0 or 1.
 * Use BOOL2BIT()    to map false to 0 and true to 1.
 * Use BOOL2INVBIT() to map false to 1 and true to 0.
 */
#define BOOL2BIT(flag)    ((flag) ? 1 : 0)
#define BOOL2INVBIT(flag) ((flag) ? 0 : 1)


/* Enable or disable an interrupt in the NVIC of Cortex-M3.
 * Set an IRQ pending or clear pending status of the interrupts
 */
#define  enableIRQ(n) do *(&NVIC_ISER + ((n) >> 5)) = 1 << ((n) & 0x1f); while (false)
#define disableIRQ(n) do *(&NVIC_ICER + ((n) >> 5)) = 1 << ((n) & 0x1f); while (false)
#define pendingIRQ(n) do *(&NVIC_ISPR + ((n) >> 5)) = 1 << ((n) & 0x1f); while (false)
#define   clearIRQ(n) do *(&NVIC_ICPR + ((n) >> 5)) = 1 << ((n) & 0x1f); while (false)
