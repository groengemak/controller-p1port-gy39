/* console.h -- Output strings to the CONS output-only serial port
 *
 * These are the structures that output character sequences with the
 * required contention handling mechanisms.
 *
 * The CONS task will work through strings one by one, and fall asleep
 * if none remains.  The strings are described by structures that hold
 * a flag indicating whether they are inuse (not free for reuse).  They
 * may also be withdrawn when they are no longer considered of interest
 * to send.  Aside that, the strings are sent in a sequentual fashion.
 *
 * When the CONS port has not detected a listener (by way of a high
 * level on the RxD port that is only sampled as a GPIO pin) there is
 * no output at all.
 *
 * From: Rick van Rein <rick@groengemak.nl>
 */


/* The console output structure.  This data is owned by a part of the
 * program that wants to generate output with it.  It may be statically
 * allocated and recycled with proper use of the flags in it.
 */
struct consput {
	//
	// The in-use flag indicates console thread ownership.
	// It is set upon submission of the structure and reset
	// after sending is done.  This flag is set by the owner
	// of the structure and reset by the console task.
	bool inuse;
	//
	// The outdated flag indicates that the console should not
	// start sending this buffer, but rather skip it.
	bool outdated;
	//
	// The character string of the buffer.  This is not unchanged
	// by the console task, so it can be a fixed buffer.
	const char *buf;
	//
	// The size of the buffer.  This is not changed by the
	// console task, so it can be a fixed buffer.
	unsigned buflen;
	//
	// The queue of consput structures, as internally managed
	// by the console thread.
	struct consput *_next;
};


/* Add a text block as a consput element to the queue.
 */
void consput (struct consput *txtblk);


/* Setup the console, especially its queue outputting task.
 */
void setupConsole (void);
