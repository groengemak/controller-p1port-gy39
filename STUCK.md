# Recover from stuck STM32F103

> *The bootloader can really get stuck.  The internal bootloader is unstable.*

**Known problems.**

 1. The flash size reports as 0 to `st-info --probe`.  Note that `st-flash` can override size.
 2. For write and erase operations on the Flash memory (write/erase), the internal RC oscillator
(HSI) must be ON.  (So when I switched that one off, trouble overwriting the flash updates got into a self-defeating loop!)

**USART bootloader.**
The system bootloader offers an access option via USART pins,
[AN5031](https://www.st.com/content/ccc/resource/technical/document/application_note/group1/b7/b5/ca/8e/93/20/42/0e/DM00389996/files/DM00389996.pdf/jcr:content/translations/en.DM00389996.pdf).
Set 

**Alternative bootloader.**  One can be found in open source projects, and
[setup as described here](https://www.electrosoftcloud.com/en/stm32f103-bootloader-and-programming/).
It involves the System Bootloader that relies on the USART interface.
Change pin header BOOT0 to position 1, that is make the farthest-from-reset jumper reach for the VBAT corner while pin header BOO1 continues to be in position 0, reach to B12, as is the default.
(Normal settings have both BOOT0 and BOOT1 in position 0.)


## Writing flash without HSI

The HSI is required for writing and erasing of flash memory.  Nice gotcha in the user manual!  How to break the cycle when my code overrules this?

**Solved.**
In [this patch](https://github.com/stlink-org/stlink/pull/1348)
on the open source `st-link` package, as added in the
[vanrein:develop branch](https://github.com/vanrein/stlink/tree/develop).


