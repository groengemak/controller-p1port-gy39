/* This is the first portion for any STM32F program.
 *
 * The _vectors point into the remainder of the code.
 * This would need more flexibility to accommodate interrrupts.
 *
 * From: Rick van Rein <rick@groengemak.nl>
 */

#include <stdint.h>


/* We will allocate a stack with room for STACKWORDS 32-bit words.
 * If this is not a defined symbol, STACKWORDS will be set to 256.
 */
#ifndef STACKWORDS
#define STACKWORDS 256
#endif

/* Forward declarations referenced in the onset of the program.
 */
void _start (void);
void main (void);
void exit (void);
void fault (void);
void nmi (void);
void instrap (void);
void memtrap (void);
void stktrap (void);
extern uint32_t _stack [];

/* Interrupt service routines that loop forever, unless overridden.
 */

#ifdef ISR_SYSTICK
void ISR_SYSTICK (void);
#else
#define ISR_SYSTICK exit
#endif

#ifdef ISR_SYSCALL
void ISR_SYSCALL (void);
#else
#define ISR_SYSCALL exit
#endif

#ifdef ISR_SYSCALL_PEND
void ISR_SYSCALL_PEND (void);
#else
#define ISR_SYSCALL_PEND exit
#endif

#ifdef ISR_EXTI_9_5
void ISR_EXTI_9_5 (void);
#else
#define ISR_EXTI_9_5 exit
#endif

/* Driver USART[123] interrupt routines split to ISR_xxx_RXD and ISR_xxx_TXD.
 */
void isr_USART1 (void);
void isr_USART2 (void);
void isr_USART3 (void);

/* Driver DMA/2 interrupt routines split to ISR_xxx_DONE.
 */
void isr_DMA1_chan7 (void);


/* The very first bit of the program must be this vector:
 *  - Initial stack pointer
 *  - Initial program counter
 *  - Various interrupt/exception service routines
 */
void *const _vectors [] = {
	(void *) &_stack[STACKWORDS-1],	// Initial SP
	(void *) _start,		// Reset routine
	(void *) nmi,			// NMI and Clock Security System
	(void *) fault,			// Fault, all classes
	(void *) memtrap,		// Memory management
	(void *) memtrap,		// Prefetch fault, Memory Access fault
	(void *) instrap,		// Undefined instruction, Illegal state
	(void *) exit,			// Reserved 0x1c
	(void *) exit,			// Reserved 0x20
	(void *) exit,			// Reserved 0x24
	(void *) exit,			// Reserved 0x28
	(void *) ISR_SYSCALL,		// SWI system service call
	(void *) exit,			// Debug Monitor
	(void *) exit,			// Reserved 0x34
	(void *) ISR_SYSCALL_PEND,	// Pendable request for system service
	(void *) ISR_SYSTICK,		// System tick timer
	(void *) exit,			// Window watchdog interrupt
	(void *) exit,			// PVD through EXTI Line interrupt
	(void *) exit,			// Tamper interrupt
	(void *) exit,			// RTC global interrupt
	(void *) exit,			// Flash global interrupt
	(void *) exit,			// RCC global interrupt
	(void *) exit,			// EXTI Line0 interrupt
	(void *) exit,			// EXTI Line1 interrupt
	(void *) exit,			// EXTI Line2 interrupt
	(void *) exit,			// EXTI Line3 interrupt
	(void *) exit,			// EXTI Line4 interrupt
	(void *) exit,			// DMA1 Channel1 global interrupt
	(void *) exit,			// DMA1 Channel2 global interrupt
	(void *) exit,			// DMA1 Channel3 global interrupt
	(void *) exit,			// DMA1 Channel4 global interrupt
	(void *) exit,			// DMA1 Channel5 global interrupt
	(void *) exit,			// DMA1 Channel6 global interrupt
	(void *) isr_DMA1_chan7,	// DMA1 Channel7 global interrupt
	(void *) exit,			// ADC1/ADC2 global interrupt
	(void *) exit,			// USB HiPri or CAN TX interrupt
	(void *) exit,			// USB LoPri or CAN RX interrupt
	(void *) exit,			// CAN RX1 interrupt
	(void *) exit,			// CAN SCE interrupt
	(void *) ISR_EXTI_9_5,		// EXTI line [9:5] interrupts
	(void *) exit,			// TIM1 Break interrupt
	(void *) exit,			// TIM1 Update interrupt
	(void *) exit,			// TIM1 Trigger and Comm interrupt
	(void *) exit,			// TIM1 Capture Compare interrupt
	(void *) exit,			// TIM2 global interrupt
	(void *) exit,			// TIM3 global interrupt
	(void *) exit,			// TIM4 global interrupt
	(void *) exit,			// I2C1 event interrupt
	(void *) exit,			// I2C1 error interrupt
	(void *) exit,			// I2C2 event interrupt
	(void *) exit,			// I2C2 error interrupt
	(void *) exit,			// SPI1 global interrupt
	(void *) exit,			// SPI2 global interrupt
	(void *) isr_USART1,		// USART1 global interrupt
	(void *) isr_USART2,		// USART2 global interrupt
	(void *) isr_USART3,		// USART3 global interrupt
	(void *) exit,			// EXTI line [15:10] interrupts
	(void *) exit,			// RTC alarm  through EXTI interrupt
	(void *) exit,			// USB wakeup through EXTI interrupt
	(void *) exit,			// TIM8 Break interrupt
	(void *) exit,			// TIM8 Update nterrupt
	(void *) exit,			// TIM8 Trigger and Comm interrupt
	(void *) exit,			// TIM8 Capture Compare interrupt
	(void *) exit,			// ADC3 global interrupt
	(void *) exit,			// FSMC global interrupt
	(void *) exit,			// SDIO global interrupt
	(void *) exit,			// TIM5 global interrupt
	(void *) exit,			// SPI3 global interrupt
	(void *) exit,			// UART4 global interrupt
	(void *) exit,			// UART5 global interrupt
	(void *) exit,			// TIM6 global interrupt
	(void *) exit,			// TIM7 global interrupt
	(void *) exit,			// DMA2 Channel1 global interrupt
	(void *) exit,			// DMA2 Channel2 global interrupt
	(void *) exit,			// DMA2 Channel3 global interrupt
	(void *) exit,			// DMA2 Channels4+5 global interrupt
};

/* Not sure how hardware can exit, except with a bang...
 */
void exit (void) {
	for (;;) ;
}

/* Fault has its own loop.
 */
void fault (void) {
	for (;;) ;
}

/* NMI has its own loop.
 */
void nmi (void) {
	for (;;) ;
}

/* Bad instruction has its own loop.
 */
void instrap (void) {
	for (;;) ;
}

/* Bad memory has its own loop.
 */
void memtrap (void) {
	for (;;) ;
}

/* Stack overflow hook, called from FreeRTOS.
 */
void stktrap (void) {
	for (;;) ;
}

/* Startup code; includes setup of the program's static region.
 * Ends up calling main().
 */
extern long unsigned int _sidata, _sdata, _edata, _sbss, _ebss;
void _start (void) {
	//
	// Copy [_sidata..] to [_sdata.._edata>
	long unsigned int *src = &_sidata;
	long unsigned int *dst = &_sdata;
	while (dst < &_edata) {
		*dst++ = *src++;
	}
	//
	// Zero-fill uninitialised static data at [_sbss.._ebss>
	dst = &_sbss;
	while (dst < &_ebss) {
		*dst++ = 0;
	}
	//
	// Start the application itself
	main ();
}

/* The stack is not normally allocated so early, but it is possible and
 * even makes sense; going out of range should not overwrite data and
 * yield unstable results, but rather cause a crash due to a wild jump.
 *
 * When FreeRTOS is used, the stack may well be much smaller, as each
 * task would get a stack of its own and FreeRTOS may be predictable.
 */
uint32_t _stack [STACKWORDS];

