/* P1 Port parsing, for "slimme meters" as we use them in the Netherlands.
 *
 * For a specification, see
 * https://www.netbeheernederland.nl/dossiers/slimme-meter-15/documenten
 * https://www.netbeheernederland.nl/_upload/Files/Slimme_meter_15_a727fce1f1.pdf
 *
 * This appears to be (a variation on) the Open Metering System,
 * https://oms-group.org/en/open-metering-system/oms-specification
 *
 * From: Rick van Rein <rick@groengemak.nl>
 */


#if HOST_EXTRAS
#include <stdlib.h>
#include <stdio.h>
#endif
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#if HOST_EXTRAS
#include <unistd.h>
#endif

#include <driver.h>


enum cosemClass {
	CTAG_NULL = 0,
	CTAG_BOOLEAN = 3,
	CTAG_BITSTRING = 4,
	CTAG_DOUBLE64 = 5,
	CTAG_UDOUBLE64 = 6,
	CTAG_FLOAT = 7,
	CTAG_OCTETSTRING = 9,
	CTAG_VISIBLESTRING = 10,
	CTAG_BCD = 11,
	CTAG_INT16 = 15,
	CTAG_INT32 = 16,
	CTAG_UINT16 = 17,
	CTAG_UINT32 = 18,
	CTAG_INT64 = 20,
	CTAG_UINT64 = 21,
	CTAG_ENUM = 22,
	CTAG_FLOAT32 = 23,
	CTAG_FLOAT64 = 24,
};

enum cosemFormat {
	CFMT_EMPTY = 0,
	CFMT_In = 1,
	CFMT_Sn = 2,
	CFMT_Fn_x_y = 3,
	CFMT_In_Fn_x_y = 4,
};

struct obisClass {
	const char *id;		// Strings like "1-0:1.8.1"
	const char *name;	// Descriptive string
	const char *opt_unit;	// Strings like "kWh" read from meter
	const char *out_unit;	// Strings like "Wh" in output
	uint8_t attr;		// Like 2 == Value
	uint8_t classID;	// Like 3 == Register
	uint8_t format;		// Like 6 = F9(3,3) with n=9, x=3, y=3 below
	uint16_t n;	 	// Length n
	uint8_t x, y;		// Decimals x, y after dot
	uint16_t regnr;		// Modbus input register 3xxxx (first)
};

//TODO// Add register numbers/lengths, and match against Modbus request
const struct obisClass obis [] = {
	{ "1-0:1.8.1", "E.recv.1", "kWh", "Wh", 2,3,6, 9,3,3 },
	{ "1-0:1.8.2", "E.recv.2", "kWh", "Wh", 2,3,6, 9,3,3 },
	{ "1-0:2.8.1", "E.send.1", "kWh", "Wh", 2,3,6, 9,3,3 },
	{ "1-0:2.8.2", "E.send.2", "kWh", "Wh", 2,3,6, 9,3,3 },
	{ "1-0:1.7.0", "P.recv", "kW", "W", 2,3,18, 5,3,3 },
	{ "1-0:2.7.0", "P.send", "kW", "W", 2,3,18, 5,3,3 },
	{ "1-0:32.7.0", "U.recv.L1", "V", "dV", 2,3,18, 4,1,1 },
	{ "1-0:52.7.0", "U.recv.L2", "V", "dV", 2,3,18, 4,1,1 },
	{ "1-0:72.7.0", "U.recv.L3", "V", "dV", 2,3,18, 4,1,1 },
	{ "1-0:31.7.0", "I.recv.L1", "A", "A", 2,3,18, 3,0,0 },
	{ "1-0:51.7.0", "I.recv.L2", "A", "A", 2,3,18, 3,0,0 },
	{ "1-0:71.7.0", "I.recv.L3", "A", "A", 2,3,18, 3,0,0 },
	{ "1-0:21.7.0", "P.recv.L1", "kW", "W", 2,3,18, 5,3,3 },
	{ "1-0:41.7.0", "P.recv.L2", "kW", "W", 2,3,18, 5,3,3 },
	{ "1-0:61.7.0", "P.recv.L3", "kW", "W", 2,3,18, 5,3,3 },
	{ "1-0:22.7.0", "P.send.L1", "kW", "W", 2,3,18, 5,3,3 },
	{ "1-0:42.7.0", "P.send.L2", "kW", "W", 2,3,18, 5,3,3 },
	{ "1-0:62.7.0", "P.send.L3", "kW", "W", 2,3,18, 5,3,3 },
	// End marker:
	{ NULL, NULL, 0, 0, 0, 0 }
};

/* TODO: This should be modified for char-by-char processing.
 */
static bool obismatch (const char *candidate, const char *parsed, int parsedlen) {
	while (parsedlen-- > 0) {
		if (*parsed++ != *candidate++) {
			return false;
		}
	}
	return (*candidate == '\0');
}

bool debug = false;

const char *lineptr = NULL;

const char *idptr = NULL;
unsigned idlen = 0;

const char *valptr = NULL;

const struct obisClass *obi = NULL;
bool ignore = true;
uint64_t fpvalue = 0;
uint8_t fpdecimals = 0;
bool fpdone = false;
bool fpstxerr = false;
bool fpstxok = false;
int ival = 0;
uint8_t dval;

const char *tmptxt = NULL;

uint16_t csum = 0;

//TODO// Match the request and only write what it wants
uint16_t busreg [20];

%%{

machine p1port;

cr = '\r' ;
lf = '\n' ;
crlf = cr? lf ;

hex4dig = (
	  [0-9]
		${ dval = *p - '0'     ; }
	| [a-f]
		${ dval = *p - 'a' + 10; }
	| [A-f]
		${ dval = *p - 'A' + 10; }
	)
		${ ival <<= 4; ival += dval; }
	;
hex8 = hex4dig.hex4dig
		>{ ival = 0; }
	;
hex16 = hex4dig.hex4dig.hex4dig.hex4dig
		>{ ival = 0; }
	;
hexstr = hex8*
		// ${ ...process...char...; }
	;

obisID = ( [01] . '-' . [01234] . ':' . [0-9]+ . '.' [0-9]+ . '.' . [0-9]+ )
	>{ idptr = p; }
	%{ idlen = (unsigned) (p - idptr);
	   obi = obis;
	   ignore = false;
	   while (!obismatch (obi->id, idptr, idlen)) {
		obi++;
		ignore = (obi->id == NULL);
		if (ignore) {
#if HOST_EXTRAS
			if (debug) {
				fprintf (stderr, "Failed to find OBIS identity %.*s\n", (unsigned) (p-idptr), idptr);
			}
#endif
			break;
		}
	   }
	};

timestamp = '(' . [0-9]{12}[SW] . ')';

opt_fpvalue = (
	[0-9]* 
		${ fpvalue = fpvalue * 10 + *p - '0'; }
	. ( '.'
	  . [0-9]*
		${ if (!fpdone) {
			fpvalue = fpvalue * 10 + *p - '0';
			if (fpdecimals == 0) {
				fpdone = true;
			} else {
				fpdecimals--;
			}
		   }
		}
	  )?
	)
		>{ fpvalue = 0; fpstxerr = false; fpdone = false; fpdecimals = obi->y; }
		%{
			while (fpdecimals-- > 0) {
				fpvalue *= 10;
			}
		}
	. [^0-9]*
		${ fpstxerr = true; }
	;

value = [^(*)]+ & opt_fpvalue
	%{ if ((obi->format == 6) || (obi->format == 18)) {
		ignore = ignore || fpstxerr;
	   }
	}
	%{ switch (obi->format) {
	   case 0:
		/* Empty */
		ignore = true;
		break;
	   case 6:
	   case 18:
		/* Fixed-point representations must have enough decimals */
		ignore = fpstxerr || (tmptxt != p);
		break;
	   default:
		/* Unrecognised types */
		ignore = true;
	   }
	};

unit = [^(*)]+
	>{ tmptxt = obi->opt_unit; }
	${ ignore = ignore || (*p != *tmptxt++); }
	%{ ignore = ignore || (*p != ')'); }
	;

measurement = '(' . value . ( '*' . unit )? . ')'
	;

generic_num = [0-9] | '10';

generic = '(' . generic_num . ')' . '('.obisID.')' . ( timestamp . measurement )+
	;

# Non-deterministic backtracking: timestamp | measurement
specific = timestamp? . measurement;

mbusTime = '(' . [0-9]{8} . 'W' . ')';

# Non-deterministic backtracking: specific | generic
dataline = ( obisID . ( specific | generic ) . crlf )
	>{ lineptr = p; }
	%{ if (!ignore) {
#if HOST_EXTRAS
		printf ("%s = %f %s\n", obi->name, value, obi->out_unit);
#endif
		switch (obi->n) {
		case 6:
			/* Unsigned double long */
			*(uint64_t*) &busreg [obi->regnr-30000] = (uint64_t) fpvalue;
			break;
		case 18:
			/* Unsigned long */
			*(uint32_t*) &busreg [obi->regnr-30000] = (uint32_t) fpvalue;
			break;
		default:
			break;
		}
		ignore = true;
	   } else {
#if HOST_EXTRAS
		if (debug) {
			fprintf (stderr, "Ignored %.*s", (unsigned) (p-lineptr), lineptr);
		}
#endif
	   }
	};

header = (any - cr - lf)+ . crlf
	;

checksum = '!' . hex16
	;

telegram := ( '/' . header+ . crlf . dataline+ )
	>{ csum = 0; }
	${ csum += cs; }
	. checksum
	. crlf
	;

}%%


const char telegram [] = "/ISk5\2MT382-1000\r\n"
	"\r\n"
	"1-3:0.2.8(50)\r\n"
	"0-0:1.0.0(101209113020W)\r\n"
	"0-0:96.1.1(4B384547303034303436333935353037)\r\n"
	"1-0:1.8.1(123456.789*kWh)\r\n"
	"1-0:1.8.2(123456.789*kWh)\r\n"
	"1-0:2.8.1(123456.789*kWh)\r\n"
	"1-0:2.8.2(123456.789*kWh)\r\n"
	"0-0:96.14.0(0002)\r\n"
	"1-0:1.7.0(01.193*kW)\r\n"
	"1-0:2.7.0(00.000*kW)\r\n"
	"0-0:96.7.21(00004)\r\n"
	"0-0:96.7.9(00002)\r\n"
	"1-0:99.97.0(2)(0-0:96.7.19)(101208152415W)(0000000240*s)(101208151004W)(0000000301*s)\r\n"
	"1-0:32.32.0(00002)\r\n"
	"1-0:52.32.0(00001)\r\n"
	"1-0:72.32.0(00000)\r\n"
	"1-0:32.36.0(00000)\r\n"
	"1-0:52.36.0(00003)\r\n"
	"1-0:72.36.0(00000)\r\n"
	"0-0:96.13.0(303132333435363738393A3B3C3D3E3F303132333435363738393A3B3C3D3E3F303132333435363738393A3B3C3D3E3F303132333435363738393A3B3C3D3E3F303132333435363738393A3B3C3D3E3F)\r\n"
	"1-0:32.7.0(220.1*V)\r\n"
	"1-0:52.7.0(220.2*V)\r\n"
	"1-0:72.7.0(220.3*V)\r\n"
	"1-0:31.7.0(001*A)\r\n"
	"1-0:51.7.0(002*A)\r\n"
	"1-0:71.7.0(003*A)\r\n"
	"1-0:21.7.0(01.111*kW)\r\n"
	"1-0:41.7.0(02.222*kW)\r\n"
	"1-0:61.7.0(03.333*kW)\r\n"
	"1-0:22.7.0(04.444*kW)\r\n"
	"1-0:42.7.0(05.555*kW)\r\n"
	"1-0:62.7.0(06.666*kW)\r\n"
	"0-1:24.1.0(003)\r\n"
	"0-1:96.1.0(3232323241424344313233343536373839)\r\n"
	"0-1:24.2.1(101209112500W)(12785.123*m3)\r\n"
	"!EF2F\r\n";


%%write data;


int main (int argc, char *argv []) {
#if HOST_EXTRAS
	if (getopt (argc, argv, "d") == 'd') {
		debug = true;
	}
#endif
	int cs = 0;
	const char *p = telegram;
	const char *pe = telegram + sizeof (telegram) - 1;
	%%write init;
	%%write exec;
	bool ok = (cs >= p1port_first_final);
	if (!ok) {
#if HOST_EXTRAS
		printf ("\n*** SYNTAX ERROR ***\n");
#endif
	} else if (p != pe) {
#if HOST_EXTRAS
		printf ("\n*** TRAILING GARBAGE ***\n");
#endif
		ok = false;
	}
#if HOST_EXTRAS
	if (!ok) {
		fprintf (stderr, "Terminated at \"%s\"\n", p);
	}
#endif
	return (ok ? 0 : 1);
}
