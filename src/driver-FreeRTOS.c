/* Driver code for FreeRTOS
 *
 * Callbacks as described in:
 * https://www.freertos.org/a00110.html
 *
 * From Rick van Rein <rick@groengemak.nl>
 */


#include <stdint.h>
#include <string.h>

#include <FreeRTOS.h>
#include <task.h>



/* Generic smaller memset() than in newlib, without non-existing vmula.i8 insn.
 */
void *memset (void *s, int c, size_t n) {
	uint8_t *u = (uint8_t*) s;
	//
	// Write a first part with one byte at a time
	if (n >= 3) {
		int cpre = 0x03 & - (intptr_t) u;
		while (cpre-- > 0) {
			*u++ = c;
			n--;
		}
	}
	//
	// Write the main portion in 32-bit chunks
	uint32_t *u32 = (uint32_t*) u;
	uint32_t c32 = 0x01010101 * (uint8_t) c;
	while (n >= 4) {
		*u32++ = c32;
		n -= 4;
	}
	u = (uint8_t*) u32;
	//
	// Finish the last bit manually
	while (n-- > 0) {
		*u++ = c;
	}
	//
	// Produce the return value
	return s;
}



/* Generic small memcpy() than in newlib, without non-existing vmula.i8 insn.
 */
void *memcpy (void *d, const void *s, size_t n) {
	uint8_t *src = (uint8_t*) s;
	uint8_t *dst = (uint8_t*) d;
	//
	// Write with one byte at a time
	while (n-- > 0) {
		*dst++ = *src++;
	}
	//
	// Produce the return value
	// We need to discard "const" because C is too stupid for it
	return (void*) s;
}



/* configSUPPORT_STATIC_ALLOCATION is set to 1, so the application must provide an
implementation of vApplicationGetIdleTaskMemory() to provide the memory that is
used by the Idle task. */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer,
                                    StackType_t **ppxIdleTaskStackBuffer,
                                    uint32_t *pulIdleTaskStackSize )
{
/* If the buffers to be provided to the Idle task are declared inside this
function then they must be declared static - otherwise they will be allocated on
the stack and so not exists after this function exits. */
static StaticTask_t xIdleTaskTCB;
static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

    /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
    state will be stored. */
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

    /* Pass out the array that will be used as the Idle task's stack. */
    *ppxIdleTaskStackBuffer = uxIdleTaskStack;

    /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
    Note that, as the array is necessarily of type StackType_t,
    configMINIMAL_STACK_SIZE is specified in words, not bytes. */
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
/*-----------------------------------------------------------*/

/* configSUPPORT_STATIC_ALLOCATION and configUSE_TIMERS are both set to 1, so the
application must provide an implementation of vApplicationGetTimerTaskMemory()
to provide the memory that is used by the Timer service task. */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer,
                                     StackType_t **ppxTimerTaskStackBuffer,
                                     uint32_t *pulTimerTaskStackSize )
{
/* If the buffers to be provided to the Timer task are declared inside this
function then they must be declared static - otherwise they will be allocated on
the stack and so not exists after this function exits. */
static StaticTask_t xTimerTaskTCB;
static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

    /* Pass out a pointer to the StaticTask_t structure in which the Timer
    task's state will be stored. */
    *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

    /* Pass out the array that will be used as the Timer task's stack. */
    *ppxTimerTaskStackBuffer = uxTimerTaskStack;

    /* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
    Note that, as the array is necessarily of type StackType_t,
    configTIMER_TASK_STACK_DEPTH is specified in words, not bytes. */
    *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}


/**
 * task.h
 * @code{c}
 * void vApplicationStackOverflowHook( TaskHandle_t Task char *pcTaskName);
 * @endcode
 *
 * The application stack overflow hook is called when a stack overflow is detected for a task.
 *
 * Details on stack overflow detection can be found here: https://www.FreeRTOS.org/Stacks-and-stack-overflow-checking.html
 *
 * @param xTask the task that just exceeded its stack boundaries.
 * @param pcTaskName A character string containing the name of the offending task.
 */
void vApplicationStackOverflowHook( TaskHandle_t xTask,
                                        char * pcTaskName ) {
	void stktrap (void);
	stktrap ();
}
