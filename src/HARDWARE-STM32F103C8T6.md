# Hardware for P1 Port and GY-39 support

This software is built for the STM32F103C8T6 microcontroller, for which devkits are easy to get:

  * Arm 32-bit Cortex-M3 CPU core on 48 MHz
  * Packaged as LQFP-48
  * Flash 64 kB, RAM 20 kB
  * Connects over 3 USARTs, 1 USB, 2 I2C, 2 SPI
  * Serial Wire Debug: Connector with 3V3, SWDIO, SWCLK, GND

The devkits are informally known as
[Blue Pill](https://jeelabs.org/article/1649a/)
and it follows
[this schema](https://jeelabs.org/img/2016/STM32F103C8T6-DEV-BOARD-SCH.pdf).

I did find one Blue Pill board suddenly reporting under `st-info --probe` that it had
[flash: 0](https://alexbirkett.github.io/microcontroller/2019/03/30/flash_bluepill_using_ST_link.html).
Probing worked but unlocking did not; OpenOCD failed to halt the chip.


## Special notes about this chip

Some in comparison to ESP32, which was an earlier consideration (and still an alternative design).

  * It cannot invert the serial input from the P1 Port, so add a BC557 for it
  * It can pull-up or pull-down input lines, so remove those from the diagram
  * Pull down the GPI,CONS line on PB1
  * Pull down the USART3,RX line on PB11; but spec.Table 24 allows only pull-up
  * Alternate the USART1,TX line on PA9/posA and on PB6/posB to send STOP
  * Note that I2C cannot be used to stretch meters to the GY-39 sensors
  * Remap USART1 between posA and posB to accommodate two GY-39 sensors
  * We have channels to spare: 1x USB, 2x SPI, 1x I2C; perhaps add a display?


## Hardware mapping

  * PA0-WKUP = 
  * PA1 = GPO,DREQ
  * PA2 = USART2_TX = Modbus-TxD
  * PA3 = USART2_RX = Modbus-RxD
  * PA4 = [SPI1] = SEL0
  * PA5 = [SPI1] = SEL1
  * PA6 = [SPI1] = SEL2
  * PA7 = [SPI1] = SEL3
  * PA8 = GPI,CONS
  * PA9  = posA.USART1_TX = GY39-Front-TxD
  * PA10 = posA.USART1_RX = GY39-Front-RxD
  * PA11 = USBDM
  * PA12 = USBDP
  * absent.PA13 = SWDIO
  * absent.PA14 = SWCLK
  * PA15 = SEL4
  * PB0 = SEL5
  * PB1 = SEL6
  * PB2 = BOOT1 = header-toggle-next-to-RESET
  * noalt.BOOT0 = header-toggle-faraway-RESET
  * PB3 = SEL7
  * PB4 = SEL8
  * PB5 = SEL9
  * PB6 = posB.USART1_TX = GY39-Back-TxD
  * PB7 = posB.USART1_RX = GY39-Back-RxD
  * PB8 = [remap.I2C1]
  * PB9 = [remap.I2C1]
  * PB10 = USART3_TX = Console-TxD
  * PB11 = USART3_RX = P1-Port-RxD
  * PB12 = [SPI2] = SEL10
  * PB13 = [SPI2] = SEL11
  * PB14 = [SPI2] = SEL12
  * PB15 = [SPI2] = SEL13
  * PC13-TAMPER-RTC = BoardLED
  * PC14 = [OSC32_IN ] = SEL14
  * PC15 = [OSC32_OUT] = SEL15
  * [PD0] = OSC_IN
  * [PD1] = OSC_OUT

**Note:** OSC_IN / _OUT are connected on the devboard so PD0 and PD1 are taken; the OSC32_IN / _OUT pins are for a 32.768 kHz-oscillator which is not connected on the devboard, so they can be used as SEL14 and SEL15.  That is a nice round figure, so let's stick to that until someone gets desparate and daring enough to use even more (perhaps at the expense of more amplification hardware).

**Note:** NRST is probably connected to the reset button on my board.  The pin has no other functions.

**Note:** There is a method of "bit banding" to set and reset single bits in SRAM and registers on ARM Cortex-M3.  This makes it easier to update individual input/output signals.  The bit-banded area is used with an offset of 32 times the byte offset in the original domain plus 4 times the bit number.  The values are represented as 1 or 0.  The SRAM starts at 0x20000000 with bit-banding at 0x22000000 and registers start at 0x40000000 with bit-banding at 0x42000000.  In both cases, an area of 1MB maps onto a 32MB area.


## USART on a Budget

We allocated as follows, with USART1 being more flexible:

  * USART1 communicates with GY-39 sensors, multiple if need be
  * USART2 reads from the P1 Port and writes to the console
  * USART3 communicates over Modbus

It is possible to configure the GY-39 sensors for I2C or for RS-232 at 3V3-level, but I2C will not work over lengths of wire.  RS-232 is better over meters, and may even work for very long stretches with RS232-RS486 converters for symmetrical signaling.

The GY-39 can be configured to send measurements when they come up, and their energy use is so low that they can easily be power-fed from a GPIO pin.  This allows a full bus of such sensors, provided that TxD is pulled low when the chip is inactive.

The cheaper solution for two GY-39 is to use the two locations for USART1, named posA and posB after the GPIO port that contains the RxD and TxD signals.  This saves components but that restricts it to two GY-39 sensors.  Let's ignore any line impedance and reflection problems and go for the logical highest value; but both length and spreading call for a low speed of (the default) 9600 bps.  At 3+14=17 bytes data to exchamge 10 bits each, this can be done 56 times a second, more than we have GPIO pins (or even chip pins on the LQFP-48 package).  When we adopt measurement periods of 5 minutes, we also will not have timing-critical problems with other sensors.

RS-485 converters may need to charge up, but having two serial legs (posA and posB) makes it possible to work in parallel.  Both legs can have an endpoint activated and switch to the next SEL signal (or switch off) after having received a response.  A failure to boot in time will also be handled automatically with resending of the request.  So, while there is room for allowing the fastest switch times, resends consider converter problems like any other line problem.

We can sense how it has been connected, and accordingly allocate Modbus registers.  With posA.USART1_TX and posB.USART1_TX configured as inputs with mild pull-up, it is possible to toggle the various SEL signals on free GPIO output pins, and see whether this pulls either (or perhaps both) TX pins along with it.  If so, welcome requests for the Modbus registers for the respective pin.  Alternatively, have a numbering scheme that drives the outputs, with even numbers on posA and odd numbers on posB, and signal one of the free GPIO pins as the respective SEL signal; in this way, it all comes down to software configuration on the Modbus master, which is necessary anyway.

Note: USART2 and USART3 on this microcontroller must be used in the given locations, but USART1 can move between PA2/PA3 and PB6/PB7 and, as we cannot span meters of I2C to reach the GY-39 sensors, we may connect these two serial port locations to the two GY-39 sensors and remap the driver between two external connections!


## Flashing STM32F Microcontrollers

  * The package `stlink-tools` contains utilities: `st-info`, `st-util`, `st-trace` and `st-flash`; use it if you have an ST-LINKv2 device that connects to SWDIO/SWCLK; an additional package `stlink-gui` offers the same functions over a GUI program `stlink-gui`.
  * The package `stm32flash` contains utility `stm32flash` for in-system programming over I2C or RS-232.


## Using the ST-Linkv2 dongle

Connect the STM32F103-devboard over 3V3, SWDIO, SWCLK, GND.

Run `st-info` with the variuos options to retrieve information about the attached device.

Run `stlink-gui` to exchange device memory with files.  Use this for backups and for replacing the program with a file holding a binary.  For better automation, use `st-flash read FILE ADDR SIZE` to retrieve flash contents or `st-flash write FILE ADDR` to replace it.

For the given number, the backup is made with `st-flash read flashdump.bin 0x08000000 0x10000` and
for this same number, the write to flash with `st-flash write flashnew.bin 0x08000000`

Run `st-util` in one process, then start `gdb-multiarch` and give the command `target remote :4242` to take control over the runtime.  Load the ELF file with `symbol-file ...` to see and single-step the C source, possibly switch to assembly with `layout asm` and `si` for single-stepping.  It is lovely to see the Microcontroller zooming in on the `str` instruction that makes all the difference!


