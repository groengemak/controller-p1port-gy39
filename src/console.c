/* console.c -- Output strings to the CONS output-only serial port
 *
 * These are the structures that output character sequences with the
 * required contention handling mechanisms.
 *
 * The CONS task will work through strings one by one, and fall asleep
 * if none remains.  The strings are described by structures that hold
 * a flag indicating whether they are inuse (not free for reuse).  They
 * may also be withdrawn when they are no longer considered of interest
 * to send.  Aside that, the strings are sent in a sequentual fashion.
 *
 * When the CONS port has not detected a listener (by way of a high
 * level on the RxD port that is only sampled as a GPIO pin) there is
 * no output at all.
 *
 * From: Rick van Rein <rick@groengemak.nl>
 */


#include <stdbool.h>

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

#include <driver.h>
#include <console.h>


/* An initial message shown after [mid-session] reboot.
 */
static char newcons_msg [] = "\r\n\r\n*** REBOOT ***\r\n";
//
static struct consput newcons = {
	.inuse = false,
	.outdated = false,
	.buf = newcons_msg,
	.buflen = sizeof (newcons_msg)-1,
};

/* An initial message sent when the user plugs into CONS.
 */
static char newsess_msg [] = "\r\n*** WELCOME ***\r\n";
static struct consput new_session = {
	.inuse = false,
	.outdated = false,
	.buf = newsess_msg,
	.buflen = sizeof (newsess_msg)-1,
};


/* The active consput queue is internal to this module, and may be
 * modified through functions herein.
 */
static struct consput *queue_head = NULL;
static struct consput **tail_next = &queue_head;
static SemaphoreHandle_t queue_lock = NULL;
static bool queue_active = false;


/* Internal storage of task data.
 */
static TaskHandle_t console_task;
static StackType_t console_stack [64];


/* Interrupt Service Routine for CONS output completion on CONS.
 * Note: Not called for an empty output buffer, only when actually done.
 */
#ifdef ISR_CONS_TXD_DMA
void ISR_CONS_TXD_DMA (void) {
	//
	// Take note of queue_head that we just finished sending
	struct consput *qh = queue_head;
	assert (qh != NULL);
	//
	// The queue head is done and can be released
	do {
		queue_head = qh->_next;
		qh->_next = NULL;
		qh->inuse = false;
		qh = queue_head;
	//
	// In fact, anything set as outdated can also be skipped
	} while ((qh != NULL) && qh->outdated);
	//
	// Stop sending when there is no work left
	if (qh == NULL) {
		stop_send_CONS ();
		queue_active = false;
		//
		// Reinitialise the NULL pointer
		tail_next = &queue_head;
		return;
	}
	//
	// The real task is to setup for the next message
	send_CONS (qh->buf, qh->buflen);
}
#endif


/* The task main loop for console text block output.
 * We stop CONS between text blocks, which may be detected.
 */
static void constask (void *_) {
	//
	// Do nothing
	while (true) {
#if 0
		for (int i=0; i<10000; i++) {
			nopCall ();
		}
		if (new_session.inuse == false) {
			consput (&new_session);
		}
#endif
		taskYIELD ();
	}
	//
	// This task will run forever
	while (true) {
		//
		// As long as nothing is in the queue, we wait for new arrivals
		while (queue_head == NULL) {
			//
			// We suspend ourselves until a new text block arrives
			vTaskSuspend (console_task);
		}
		//
		// Fetch data, then free the lock on the queue_head
		const char *buf = queue_head->buf;
		uint16_t buflen = queue_head->buflen;
		xSemaphoreGive (queue_lock);
		//
		// Start output
		start_send_CONS ();
		//
		// Print the first element in the consput queue
		send_CONS (buf, buflen);
/* TODO:WORKAROUND: Stopped sending too fast, not effective output */
for (int i = 0; i < 10000; i++) { nopCall (); }
		//
		// Stop output, also disabling isr_cons_txd() */
		stop_send_CONS ();
		//
		// Grab the lock on the queue_head
		while (pdTRUE != xSemaphoreTake (queue_lock, 100)) {
			taskYIELD ();
		}
		//
		// Remove the head value, plus following ones that are outdated
		do {
			//
			// Release the head element
			queue_head->inuse = false;
			queue_head->_next = NULL;
			//
			// Move the queue pointer forward
			queue_head = queue_head->_next;
			//
			// Consider that this may be the end
			if (queue_head == NULL) {
				tail_next = &queue_head->_next;
				break;
			}
		} while (queue_head->outdated);
	//
	// Forever continue printing console text blocks
	}
}


/* Add a text block as a consput element to the queue.
 * It must not be inuse, and its outdated flag will be reset.
 */
void consput (struct consput *txtblk) {
	//
	// Demand that the txtblk structure is free
	assert (!txtblk->inuse);
	txtblk->inuse = true;
	txtblk->outdated = false;
	//
	// Test if the RxD side of the CONS port is high (for listening)
	static bool was_online = true;
#if 0
	bool now_online = getCONS () || risenCONS ();
#else
	bool now_online = true;
#endif
	//
	// When just coming now_online, prefix a WELCOME message
	if (now_online && !was_online) {
		//
		// Recurse once, to prefix a WELCOME
		if (!new_session.inuse) {
			consput (&new_session);
		}
		//
		// This should set queue_active
	}
	//
	// If we are not now_online, we simply do not enqueue
	if (!now_online) {
		txtblk->inuse = false;
		was_online = false;
		return;
	}
	//
	// Grab the queue handling lock
	while (pdTRUE != xSemaphoreTake (queue_lock, 100)) {
		taskYIELD ();
	}
	//
	// Enqueue the text block
	//TODO// Perhaps welcome a chain of messages
	assert (*tail_next == NULL);
	txtblk->_next = NULL;
	*tail_next = txtblk;
	tail_next = &txtblk->_next;
	//
	// If the queue is currently at rest, then start it
	if (!queue_active) {
		//
		// Setup the interrupt handler
		queue_active = true;
		start_send_CONS ();
		//
		// Setup for sending the new queue_head
		//TODO// May end up as double action
		send_CONS (queue_head->buf, queue_head->buflen);
	}
	//
	// Store the current now_online value for later
	was_online = now_online;
	//
	// Free the queue handling lock
	xSemaphoreGive (queue_lock);
}


/* Setup the console, especially its queue outputting task.
 */
void setupConsole (void) {
	//
	// Create a semaphore to control access to the queue_head
	// and use a binary semaphore, which starts being locked
	queue_lock = xSemaphoreCreateBinary ();
	xSemaphoreGive (queue_lock);
	//
	// Setup the reboot message for the thread
	newcons.inuse = false;
	newcons.outdated = false;
	newcons._next = NULL;
	queue_head = &newcons;
	tail_next  = &newcons._next;
	queue_active = true;
	start_send_CONS ();
	send_CONS (newcons.buf, newcons.buflen);
	//
	// Create the console task
	BaseType_t rv = xTaskCreate (
			constask,
			"console",
			sizeof (console_stack) / sizeof (StackType_t),
			(void *) 0,
			1,
			&console_task);
	//
	// Require successful creation of the console task
	assert (rv == pdPASS);
}
