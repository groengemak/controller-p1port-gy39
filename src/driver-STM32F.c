/* Driver code for STM32F platforms.
 *
 * This assumes a given mapping to hardware pins, see the HARDWARE document.
 *
 * This code relies on bit-banding, such as available in Cortex-M3.
 *
 * From: Rick van Rein <rick@groengemak.nl>
 */


#include <stdint.h>
#include <stdbool.h>

#include <driver.h>
#include <STM32F/ports.h>


#define NUM_SELBITS 16

static uint32_t *const selbit [NUM_SELBITS] = {
	&BIT(GPIOA_ODR,4),
	&BIT(GPIOA_ODR,5),
	&BIT(GPIOA_ODR,6),
	&BIT(GPIOA_ODR,7),
	&BIT(GPIOA_ODR,15),
	&BIT(GPIOB_ODR,0),
	&BIT(GPIOB_ODR,1),
	&BIT(GPIOB_ODR,3),
	&BIT(GPIOB_ODR,4),
	&BIT(GPIOB_ODR,5),
	&BIT(GPIOB_ODR,12),
	&BIT(GPIOB_ODR,13),
	&BIT(GPIOB_ODR,14),
	&BIT(GPIOB_ODR,15),
	&BIT(GPIOC_ODR,14),
	&BIT(GPIOC_ODR,15),
};

static uint32_t *selbit_active [2];

static bool seen_rise_pa8;


/* Simple NOP call, to bypass optimising out active waiting loop code.
 */
void nopCall (void) {
	;
}


/* Configure the RCC with our main activities.  Switch to the HSE clock.
 */
static inline void setupRCC (void) {
	//
	// Switch on the HSE clock.  Leave HSI running.
	// Later wait for clock sync in finishRCC() below.
	BIT (RCC_CR, 16) = 1;
	//
	// Enable the clock on USART1, GPIOC, GPIOB, GPIOA, AFI
	RCC_APB2ENR = 0x0000401d;
	//
	// Enable the clock on USART3, USART2
	RCC_APB1ENR = 0x00060000;
	//
	// Enable the clock on DMA1
	RCC_AHBENR  = 0x00000001;
	//
	// Make sure the clocks sink in before continuing to depend on them
	REGISTER_BARRIER ();
}


/* Wait for the RCC to have stabilised on the HSE clock.
 * Disable the HSI clock once this has worked.
 */
static inline void finishRCC (void) {
	//
	// Poll for the HSE clock to be stable/active
	while (BIT (RCC_CR, 17) == 0) {
		//
		// Not there yet...
		REGISTER_BARRIER ();
	}
	//
	// Switch SYSCLK to the HSE clock; no PLL, no CLKOUT
	RCC_CFGR = 0x00000001;
	//
	// Poll for the clocks that are currently used
	while ((RCC_CFGR & 0x0000000f) != 0x00000005) { 
		//
		// Not there yet...
		REGISTER_BARRIER ();
	}
	//
	// Switch off the HSI clock
	BIT (RCC_CR, 0) = 0;
}


/* Configure GPIOA for SEL4,x,x,x,x,posA.RxD,posA.TxD,CONS ; SEL3,SEL2,SEL1,SEL0,RxD,TxD,DREQ,x
 */
static inline void setupGPIOA (void) {
	//
	// Reset pull-down bits in ODR for PA8=GPI,CONS
	//RESET-DEFAULT// BIT (GPIOA_ODR,8) = 0;
	//
	// Reset SELn output bits on PA15,PA6,PA6,PA5,PA4;
	// add a pull-down for GPI,CONS on PA8
	//RESET-DEFAULT// GPIOA_ODR &= 0xffff7e0f;
	//
	// Setup CRH for SEL4,x,x,x,x,RxD,TxD,CONS
	// so PushPullOut,x,x,x,x,FloatIn,PushPullAlt,PulledIn
	GPIOA_CRH = (GPIOA_CRH & 0x0ffff000) | 0x10000498;
	//
	// Setup CRL for SEL3,SEL2,SEL1,SEL0,RxD,TxD,DREQ,x
	// so PushPullOut,PushPullOut,PushPullOut,PushPullOut,FloatIn,PushPullAlt,PushPullOut,x
	GPIOA_CRL = (GPIOA_CRL & 0x0000000f) | 0x11114910;
}

/* Configure GPIOB for SEL13,SEL12,SEL11,SEL10,RxD,TxD,x,x ; posB.RxD,posB.TxD,SEL9,SEL8.SEL7,x,SEL6,SEL5
 */
static inline void setupGPIOB (void) {
	//
	// Reset SELn in ODR output bits on PB15,PB14,PB13,PB12,PB5,PB4,PB3,PB1,PB0
	// Set   SELn in ODR output bits on PB6
	GPIOB_ODR = (GPIOB_ODR & 0xffff0f3b) | 0x00000040;
	//
	// Setup CRH for SEL13,SEL12,SEL11,SEL10,RxD,TxD,x,x
	// so PushPullOut,PushPullOut,PushPullOut,PushPullOut,FloatIn,PushPullAlt,x,x
	GPIOB_CRH = (GPIOB_CRH & 0x000000ff) | 0x11114900;
	//
	// Setup CRL for [RxD],[TxD],SEL9,SEL8,SEL7,x,SEL6,SEL5
	// so FloatIn,PulledIn,PushPullOut,PushPullOut,PushPullOut,x,PushPullOut,PushPullOut,
	GPIOB_CRL = (GPIOB_CRL & 0xfffff0ff) | 0x48111011;
}

/* Configure GPIOC for SEL15,SEL14,BoardLED,x,x,x,x,x ; x,x,x,x,x,x,x,x
 */
static inline void setupGPIOC (void) {
	//
	// Reset SEL15,BoardLED/SEL14 in ODR output bits
	// and switch off the BoardLED
	//RESET//DEFAULT// GPIOC_ODR &= 0x1fff5fff
	//
	// Setup CRH for [SEL15],[SEL14],BoardLED,x,x,x,x,x
	// so PushPullOut,PushPullOut,PushPullOut,x,x,x,x,x
	GPIOC_CRH = (GPIOC_CRH & 0x000fffff) | 0x11100000;
	//
	// Skip CRL since nothing changes in the low part of GPIOC
}

/* Set the on-board LED on or off via bit-banding.
 * Note: The LED will light up when driven with "0".
 */
void boardLED (bool light) {
	BIT (GPIOC_ODR, 13) = BOOL2INVBIT (light);
}


/* Set the output signal DREQ, indicating whether the P1 Port is active.
 */
void setDREQ (bool active) {
	BIT(GPIOA_ODR,1) = BOOL2BIT (active);
}


/* Setup for SELn lines, as pointed to in selbit[] above.
 * This configures the corresponding hardware bits and
 * makes them inactive; it prepares two victims as those
 * who are currently active (so they are idempotently cleared
 * before another is activated).
 */
static inline void setupSELn (void) {
	//
	// Prepare SEL0 and SEL1 as idempotent-clearing victims
	// to avoid writing to random memory locations
	selbit_active [0] = selbit [0];
	selbit_active [1] = selbit [1];
}


/* Select or deselect one of the independent SELn signals.
 * Any GY-39 fed from the SELn line is powered up or down.
 * Glitches due to switching to the same are avoided.
 *
 * Multiple GY-39 sharing a line would clash, so a bus must be
 * constructed for such situations.  At most one even SELn and
 * one odd SELn will be activated by this routine at any time.
 *
 * Setting and clearing a line are idempotent operations.
 *
 * This has no impact on remapping the USART for GY-39 sensors.
 */
void setSELn (unsigned n, bool active) {
	//
	// Sanity check -- no silent accept for out-of-range SELn
	assert (n < NUM_SELBITS);
	//
	// Find whether this is an odd- or even-numbered SELn
	int n_odd_even = n & 1;
	//
	// Handle the previous SELn if it is not also the new SELn
	if (selbit_active [n_odd_even] != selbit_active [n]) {
		//
		// Make sure that the old SELn is off
		// (which may already be the case after setupSELn()
		*selbit_active [n_odd_even] = 0;
		//
		// Save the new SELn for later treatment as the old
		selbit_active [n_odd_even] = selbit [n];
	}
	//
	// Power on the new SELn line (which is an idempotent operation)
	// Always do this, as the old state is inactive right after selSELn()
	*selbit [n] = BOOL2BIT (active);
}


/* Remap USART1 to the desired GY-39 sensor bus (posA or posB).
 *
 * This has no bearing on the SELn signals, use setSELn() for that.
 *
 * One possible design connects the TX's from both positions and
 * RX's from both positions.  We shall work to avoid short-circuit
 * by switching the losing TX to a pull-up input before switching
 * the gaining TX to alternate push-pull output.
 */
void selGY39 (unsigned n) {
	//
	// The GY-39 sensors are on an even or odd bus, posA or posB
	bool odd = n & 1;
	//
	// The current setting may already be correct in USART1_REMAP, so
	// bit 2 in AFIO_RMAP; this bit flags if the position is now odd
	bool now_odd = BIT (AFIO_MAPR, 2);
	if (odd == now_odd) {
		return;
	}
	//
	// Dismantle the old position, by switching TxD to a pull-up input
	if (now_odd) {
		//
		// Setup PB6=posB.USART1_TX for pull-up through GPIOB_ODR
		BIT (GPIOB_ODR,6) = 1;
		//
		// Switch PB6=posB.USART1_TX from output to pull-up input
		GPIOB_CRL = (GPIOB_CRL & 0xf0ffffff) | 0x08000000;
	} else {
		//
		// Setup PA9=posA.USART1_TX for pull-up through GPIOA_ODR
		BIT (GPIOA_ODR,8) = 1;
		//
		// Switch PA9=posA.USART1_TX from output to pull-up input
		GPIOA_CRH = (GPIOA_CRH & 0xffffff0f) | 0x00000080;
	}
	//
	// Remap USART1 to the newly desired position
	BIT (AFIO_MAPR, 2) = odd;
	//
	// Configure the new position, by switching TxD to a push-pull output
	if (odd) {
		//
		// Setup PA9=posA.USART1_TX for alternative push-pull output
		GPIOA_CRH = (GPIOA_CRH & 0xffffff0f) | 0x00000090;
	} else {
		//
		// Setup PB6=posB.USART1_TX for alternative push-pull output
		GPIOB_CRL = (GPIOB_CRL & 0xf0ffffff) | 0x09000000;
	}
}


/* Configure rising edges on the CONS input signal.
 *
 * TODO: This logic does not work.  EXTI_PR stores interrupts, not events.
 *       We have not setup the corresponding interrupt mask and so there
 *       is no interrupt to commemorate in EXTI_PR.
 */
static inline void setupCONS (void) {
	//
	// If an ISR is configured, allow it being triggered
	#ifdef ISR_EXTI_9_5
	BIT (EXTI_IMR,8) = 1;
	#endif
	//
	// Setup the external interrupt control register for PA8=GPI,CONS
	AFIO_EXTICR3 &= 0xfffffff0;
	//
	// Setup PA8=GPI,CONS for rising edge
	BIT (EXTI_RTSR,8) = 1;
	//
	// Reset spurious initial detection due to edge configuration
	seen_rise_pa8 = false;
	BIT (EXTI_PR,8) = 1;
	//
	// Setup NVIC interrupt line for EXTI_9_5 (interrupt position 23)
	enableIRQ (23);
}

/* Sample the current value on CONS.
 */
bool getCONS (void) {
	//
	// Read the input pin PA8=GPI,CONS
	return BIT (GPIOA_IDR,8);
}

/* See if CONS saw a rising edge since the last time we checked.
 *
 * Avoid missing signals; detect the event and only then clear it.
 * Multiple events occurring since the last check are combined into one.
 * 
 */
bool risenCONS (void) {
	//
	// Determine if a rising edge in EXTI1 is pending for PA8=GPI,CONS
	bool retval = seen_rise_pa8;
	//
	// Reset the rising edge via EXTI1 only if we found it pending
	if (retval) {
		//
		// Since we will return it, clear the pending state
		seen_rise_pa8 = false;
	}
	//
	// Return the detected edge (race conditions simply come later)
	return retval;
}


/* Configure the USART1,2,3 interfaces.
 */
static inline void setupUSART1 (void) {
	//
	// USART1: GY-39 bus, with separate odd/even sides on the STM32
	//  - enable USART1 service with UE=1
	BIT (USART1_CR1, 13) = 1;
	//  - setup for 8N1: M=0, PCE=0, STOP=00
	BIT (USART1_CR1, 12) = 0;
	BIT (USART1_CR1, 10) = 0;
	USART1_CR2 &= 0xcfff;
	//  - setup for 9600 bps under the 8 MHz clock
	//  - the frequency divider is 833.333 -- to be rounded
	USART1_BRR = 833;
	//  - synchronise so the configuration is written
	REGISTER_BARRIER ();
	//  - enable USART1 interrupts in the NVIC
	enableIRQ (37);
}
static inline void setupUSART2 (void) {
	//
	// USART2: Read P1 Port, write console
	//  - enable USART2 service with UE=1
	BIT (USART2_CR1, 13) = 1;
	//  - setup for 8N1: M=0, PCE=0, STOP=00
	BIT (USART2_CR1, 12) = 0;
	BIT (USART2_CR1, 10) = 0;
	USART2_CR2 &= 0xcfff;
	//  - enable DMA for TxD with DMAT=1
	BIT (USART2_CR3, 7) = 1;
	//  - setup for 115200 bps under the 8 MHz clock
	//  - the frequency divider is 69.4444 -- to be rounded
	USART2_BRR = 69;
	//  - setup the peripheral address for DMA1 channel 7
	DMA1_CPAR7 = (intptr_t) &USART2_DR;
	//  - synchronise so the configuration is written
	REGISTER_BARRIER ();
	//  - enable USART2 interrupts in the NVIC
	enableIRQ (38);
	//  - enable DMA1 channel 7 interrupts in the NVIC
	enableIRQ (17);
}
static inline void setupUSART3 (void) {
	//
	// USART3: Modbus in/out
	//  - enable USART3 service with UE=1
	BIT (USART3_CR1, 13) = 1;
	//  - setup for 8N1: M=0, PCE=0, STOP=00
	BIT (USART3_CR1, 12) = 0;
	BIT (USART3_CR1, 10) = 0;
	USART3_CR2 &= 0xcfff;
	//  - setup for 9600 bps under the 8 MHz clock
	//  - the frequency divider is 833.333 -- to be rounded
	USART3_BRR = 833;
	//  - synchronise so the configuration is written
	REGISTER_BARRIER ();
	//  - enable USART3 interrupts in the NVIC
	enableIRQ (39);
}


/* Start sending to GY-39 on USART1.
 */
void start_send_GY39 (void) {
	//  - start an idle frame with the TE sequence 0,1
	BIT (USART1_CR1, 3) = 0;
	REGISTER_BARRIER ();
	BIT (USART1_CR1, 3) = 1;
	REGISTER_BARRIER ();
	//  - enable USART1 send interrupts with TXEIE=1
	BIT (USART1_CR1, 7) = 1;
}

/* Stop sending to GY-39 on USART1.
 */
void stop_send_GY39 (void) {
	//  - disable USART1 send interrupts with TXEIE=0
	BIT (USART1_CR1, 7) = 0;
	//  - stop the TE with 0
	BIT (USART1_CR1, 3) = 0;
}

/* Send a character to GY-39 on USART1.
 */
void send_GY39 (uint8_t ch) {
	//
	// Drop the character in the output data register
	USART1_DR = ch;
}

/* Start receiving from GY-39 over USART1.
 */
void start_recv_GY39 (void) {
	//  - start receiving with RE=1
	BIT (USART1_CR1, 2) = 1;
	//  - enable USART1 recv interrupts with RXNEIE=1
	BIT (USART1_CR1, 5) = 1;
}

/* Stop receiving from GY-39 over USART1.
 */
void stop_recv_GY39 (void) {
	//  - disable USART1 recv interrupts with RXNEIE=0
	BIT (USART1_CR1, 5) = 0;
	//  - stop the RE with 0
	BIT (USART1_CR1, 2) = 0;
}

/* Receive a character from GY-39 over USART1.  Return success.
 */
bool recv_GY39 (uint8_t *ch) {
	//
	// Test if RXNE signals that a character is waiting
	bool retval = BIT (USART1_SR, 5);
	//
	// Pickup the character in the data register, if any
	if (retval) {
		*ch = USART1_DR;
	}
	//
	// Return whether a character was found
	return retval;
}

/* Process a USART1 interrupt.
 */
void isr_USART1 (void) {
	//
	// Process receive requests if RXNE was raised
	if (BIT (USART1_SR, 5) == 1) {
		BIT (USART1_SR, 5) = 1;
#ifdef ISR_GY39_RXD
		void ISR_GY39_RXD (void);
		ISR_GY39_RXD ();
#endif
	}
	//
	// Process send requests if TXE was raised
	if (BIT (USART1_SR, 7) == 1) {
		BIT (USART1_SR, 7) = 1;
#ifdef ISR_GY39_TXD
		void ISR_GY39_TXD (void);
		ISR_GY39_TXD ();
#endif
	}
	//
	// Clear the USART1 interrupt flag
	REGISTER_BARRIER ();
	clearIRQ (37);
}


/* Start sending to CONS on USART2.
 */
void start_send_CONS (void) {
	//  - start an idle frame with the TE sequence 0,1
	BIT (USART2_CR1, 3) = 0;
	REGISTER_BARRIER ();
	BIT (USART2_CR1, 3) = 1;
	REGISTER_BARRIER ();
	//  - do not enable USART2 or DMA1 channel 7 interrupts
}

/* Stop sending to CONS on USART2.
 */
void stop_send_CONS (void) {
	//  - disable any DMA1 channel 7 interrupts with TCIE=0
	BIT (DMA1_CCR7, 1) = 0;
	//  - stop the TE with 0
	BIT (USART2_CR1, 3) = 0;
}

/* Send a character string to CONS on USART2.  The output is
 * sent via DMA1 controller, channel 7, with aptly low priority.
 *
 * The proper thing to do is to wait for the interrupt before
 * sending another transmission (a new one would otherwise break
 * off an ongoing transmission, which makes for ugly reading).
 */
void send_CONS (const char *txt, uint16_t txtlen) {
	//
	// Disable DMA1 channel 7 so we can configure it
	BIT (DMA1_CCR7, 0) = 0;
	REGISTER_BARRIER ();
	//
	// Clear any old completed-interrupt-flag from DMA1 channel 7
	BIT (DMA1_IFCR, 25) = 1;
	//
	// Setup the memory address
	DMA1_CMAR7 = (intptr_t) txt;
	//
	// Setup the memory length
	DMA1_CNDTR7 = txtlen;
	//
	// Enable DMA1 channel 7 with the following settings:
	//  - MEM2MEM = 0 because USART2 is a peripheral
	//  - PL = 00 for a low priority (CONS is for humans)
	//  - MSIZE = 00 for  8-bit memory
	//  - PSIZE = 01 for 16-bit peripheral
	//  - MINC = 1 to increment memory addresses
	//  - PINC = 0 to not increment the peripheral address
	//  - CIRC = 0 to disable circular mode
	//  - DIR = 1 to read from memory (and write to peripheral)
	//  - TEIE,HTIE,TCIE = 0,0,1 for interrupt on completion only
	//  - EN = 1 to enable the channel
	//
	// Note: It is harmless to keep DMA1 channel 7 active once the
	//       DMA1_CNDTR7 reaches 0, as nothing more will be done.
	//
	DMA1_CCR7 = 0x00000193;
}

/* Start receiving from the P1 Port over USART2.
 */
void start_recv_P1 (void) {
	//  - start receiving with RE=1
	BIT (USART2_CR1, 2) = 1;
	//  - enable USART2 recv interrupts with RXNEIE=1
	BIT (USART2_CR1, 5) = 1;
}

/* Stop receiving from the P1 Port over USART2.
 */
void stop_recv_P1 (void) {
	//  - disable USART2 recv interrupts with RXNEIE=0
	BIT (USART2_CR1, 5) = 0;
	//  - stop the RE with 0
	BIT (USART2_CR1, 2) = 0;
}

/* Receive a character from the P1 Port over USART2.  Return success.
 */
bool recv_P1 (uint8_t *ch) {
	//
	// Test if RXNE signals that a character is waiting
	bool retval = BIT (USART2_SR, 5);
	//
	// Pickup the character in the data register, if any
	if (retval) {
		*ch = USART2_DR;
	}
	//
	// Return whether a character was found
	return retval;
}

/* Process a USART2 interrupt.
 */
void isr_USART2 (void) {
	//
	// Process receive requests if RXNE was raised
	if (BIT (USART2_SR, 5) == 1) {
		BIT (USART2_SR, 5) = 1;
#ifdef ISR_P1_RXD
		void ISR_P1_RXD (void);
		ISR_P1_RXD ();
#endif
	}
}

/* Process a DMA1 channel 7 interrupt for CONS string send
 * completion, and trigger ISR_CONS_TXD() for it, if defined.
 */
void isr_DMA1_chan7 (void) {
	//
	// Process send requests if DMA1 raised TCIF7 completion
	if (BIT (DMA1_ISR, 25) == 1) {
		//
		// Clear the flag holding the interrupt cause
		BIT (DMA1_IFCR, 25) = 1;
		//
		// Clear the DMA1 channel 7 interrupt flag
		REGISTER_BARRIER ();
		clearIRQ (17);
		//
		// Invoke the routine, welcoming new DMA from it
#ifdef ISR_CONS_TXD_DMA
		void ISR_CONS_TXD_DMA (void);
		ISR_CONS_TXD_DMA ();
#endif
#if 0
		//
		// Clear the DMA1 channel 7 interrupt flag
		REGISTER_BARRIER ();
		clearIRQ (17);
#endif
	}
}


/* Start sending to Modbus on USART3.
 */
void start_send_Modbus (void) {
	//  - start an idle frame with the TE sequence 0,1
	BIT (USART3_CR1, 3) = 0;
	REGISTER_BARRIER ();
	BIT (USART3_CR1, 3) = 1;
	REGISTER_BARRIER ();
	//  - enable USART3 send interrupts with TXEIE=1
	BIT (USART3_CR1, 7) = 1;
}

/* Stop sending to Modbus on USART3.
 */
void stop_send_Modbus (void) {
	//  - disable USART3 send interrupts with TXEIE=0
	BIT (USART3_CR1, 7) = 0;
	//  - stop the TE with 0
	BIT (USART3_CR1, 3) = 0;
}

/* Send a character to Modbus on USART3.
 */
void send_Modbus (uint8_t ch) {
	//
	// Drop the character in the output data register
	USART3_DR = ch;
}

/* Start receiving from Modbus over USART3.
 */
void start_recv_Modbus (void) {
	//  - start receiving with RE=1
	BIT (USART3_CR1, 2) = 1;
	//  - enable USART3 recv interrupts with RXNEIE=1
	BIT (USART3_CR1, 5) = 1;
}

/* Stop receiving from Modbus over USART3.
 */
void stop_recv_Modbus (void) {
	//  - disable USART3 recv interrupts with RXNEIE=0
	BIT (USART3_CR1, 5) = 0;
	//  - stop the RE with 0
	BIT (USART3_CR1, 2) = 0;
}

/* Receive a character from Modbus over USART3.  Return success.
 */
bool recv_Modbus (uint8_t *ch) {
	//
	// Test if RXNE signals that a character is waiting
	bool retval = BIT (USART3_SR, 5);
	//
	// Pickup the character in the data register, if any
	if (retval) {
		*ch = USART3_DR;
	}
	//
	// Return whether a character was found
	return retval;
}

/* Process a USART3 interrupt.
 */
void isr_USART3 (void) {
	//
	// Process receive requests if RXNE was raised
	if (BIT (USART3_SR, 5) == 1) {
		BIT (USART3_SR, 5) = 1;
#ifdef ISR_MODBUS_RXD
		void ISR_MODBUS_RXD (void);
		ISR_MODBUS_RXD ();
#endif
	}
	//
	// Process send requests if TXE was raised
	if (BIT (USART3_SR, 7) == 1) {
		BIT (USART3_SR, 7) = 1;
#ifdef ISR_MODBUS_TXD
		void ISR_MODBUS_TXD (void);
		ISR_MODBUS_TXD ();
#endif
	}
	//
	// Clear the USART3 interrupt flag
	REGISTER_BARRIER ();
	clearIRQ (39);
}


#if 0
/* Send data out over USART1, USART2 or USART3.
 */
void serialTX (uint8_t *dev, uint8_t *msg, unsigned msglen) {
	//
	// Sanity checking under DEBUG, to avoid quietly falling through
	assert ((dev == USART1) || (dev == USART2) || (dev == USART3));
	//
	// Send the string to the requested device
	"TODO";
}
#endif

#if 0
/* Poll to read a data byte from USART1, USART2 or USART3.
 *
 * When a  byte is pending, store it in *data and return true .
 * When no byte is pending, don't alter *date and return false.
 */
bool serialRXpoll (uint8_t *dev, uint8_t *data) {
	//
	// Sanity checking under DEBUG, to avoid quietly falling through
	assert ((dev == USART1) || (dev == USART2) || (dev == USART3));
	//
	// Receive the byte from the requested device
	"TODO";
}
#endif


/* Interrupt Service Routine: ISR_EXTI_9_5 for rising edge on CONS.
 * The ISR name must be explicitly defined in symbol ISR_EXTI_9_5.
 */
#ifdef ISR_EXTI_9_5
void ISR_EXTI_9_5 (void) {
	//
	// Check if this interrupt is due to EXTI_8
	if (BIT (EXTI_PR,8)) {
		//
		// Take note of this event having happened
		seen_rise_pa8 = true;
		//
		// Acknowledge the interrupt to stop triggering it
		BIT (EXTI_PR,8) = 1;
		clearIRQ (23);
	}
}
#endif


/* General hardware setup.
 */
void setupDriver (void) {
	//
	// General setup
	setupRCC ();
	assert (RCC_APB2ENR != 0);
	assert (RCC_APB1ENR != 0);
	assert (RCC_AHBENR  != 0);
	//
	// GPIO setup, still a bit general
	setupGPIOA ();
	setupGPIOB ();
	setupGPIOC ();
	//
	// Specific function setup
	setupCONS ();
	setupSELn ();
	setupUSART1 ();
	setupUSART2 ();
	setupUSART3 ();
	//
	// Wait until the clock is stable
	finishRCC ();
	//
	// As the very last step, enable interrupts
	REGISTER_BARRIER ();
#ifdef DRIVER_ENABLES_INTERRUPTS
	__asm__ ("cpsie i");	/* TODO: specific to constest on STM32F */
#endif
}
