# P1-port and GY-39 sampling over Modbus via ESP32

> *This is a contrib that starts tasks for P1 port scanning and I2C sensor sampling when so requested over Modbus.  Modbus is initially designed for RS-485, but may in future also be made available over WiFi.*


The connections are as follows:

  * Modbus connects via an RS-485 converter to RX0 and TX0
  * P1-port feeds the circuit, which it can do up to 250 mW
  * P1-port is sampled when GPO,DREQ goes up, its inverted signal is read over /RX2
  * Console output from TX2 is sent to the standard stereo jack
  * Console activity is sampled on GPI,CONS as that is high on inactive RxD
  * I2C is read and written over SCK/SDA with a feed that hopefully spans the distance

The software has the following tasks:

  * **Console output** is sent over GPI,CONS and "round up" when it is sending data.  That is, while inactive/low, setup for rising edge and sample the level once more; when rising, disable the edge but confirm before every send if it might be down.  If it seems down, setup for rising edge, then send and afterwards if it has not triggered a rising edge, disable the console.

  * **Sample the P1 port** if so requested over Modbus.  Have /RX2 setup for inverted reading and raise GPO,DREQ when Modbus asks for data.  Retreive the frame or timeout after 2.5s for 3 reception attempts, and lower GPO,DREQ when done with the frame or timeout.  The frame is parsed, stored into the Modbus answer and submitted accordingly over Modbus.

  * **Sample over I2C** if so requested over Modbus.  Send the GY-39 temperature/humidity sensors the appropriate requests over I2C, retrieve the result and reply over Modbus.

  * **Listen to Modbus over RS-485** for incoming requests, by listening to RX0 for inquiries, aware of the bus timeout.  Only recognise the address for this device, and map register ranges to the various backends.  Transmit the output over TX0.

  * **Listen to Modbus over WiFi** as a possible future extension.

The console works as follows:

  * This is a 3.5mm stereo jack socket, configured for RS-232 at 3V3 level, as a DTE.
  * On this socket, tip is the TxD output, ring is the RxD input which is not used but to sample for a high level, and sleeve is SGND (signal ground).
  * More on [this "standard"](http://internetwide.org/blog/2017/12/26/hacker-standard-rs232-plug.html)
    which is as arbitrary as standards come, but very practical for easy hardware debugging.
  * With nothing plugged in, its RxD is pulled low and there will be no output to TxD; the device does not receive data over this input (yet).
  * With a connector plugged in, its RxD would be always, mostly or regularly high and this will be detected; the device will log data to the TxD wire.


