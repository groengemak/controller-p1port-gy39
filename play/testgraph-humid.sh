#!/bin/sh
#
# Draw a graph over the last day, args [[RRDfile] PNGfile]
#
# From: Rick van Rein <rick@openfortress.nl>


if [ $# -eq 0 ]
then
	RRDFILE="/tmp/test.rrd"
	PNGFILE="/tmp/test-humid.png"
elif [ $# -eq 1 ]
then
	RRDFILE="/tmp/test.rrd"
	PNGFILE="$1"
elif [ $# -eq 2 ]
then
	RRDFILE="$1"
	PNGFILE="$2"
else
	echo >&1 "Usage: $0 [[file.rrd] file.png]"
	exit 1
fi

rrdtool graph "$PNGFILE" --end now --start now-86400s --width 1024 --height 500 "DEF:h=$RRDFILE:humid:AVERAGE" 'LINE1:h#00ff00:Humidity [%]\l'

