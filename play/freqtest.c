/* freqtest.c -- Produce frequencies on I/O pins.
 *
 * Count in 8 bits and output, to reveal halving frequencies,
 *  - PA0 at 64 Hz until PA7 at 0.5 Hz or "1s on, 1s off"
 *  - PB8..PB15 mirror PA0..PA7 except with Hi-Z second halves
 *  - For instance, PB15 is "0.5s on, 0.5s Hi-Z, 0.5s off, 0.5s Hi-Z"
 *  - The latter signals may show RC-curves on an oscilloscope
 *  - The latter signals may be probe-measured as 1 / 0 / floating
 *
 * From: Rick van Rein <rick@groengemak.nl>
 */


#include <stdint.h>
#include <stdbool.h>

#include <driver.h>
#include <STM32F/ports.h>	/* Nasty specifics, only here for playing */


#include <FreeRTOS.h>
#include <timers.h>



/* The counter value, dedicated to be updated by the tsr_count() routine.
 */
static uint16_t counter;


/* This is the Timer Service Routine for the counter.  It will add one and
 * output the result to PA0..PA7 as well as to PB8..PB15 and it will make
 * PC13 follow along with PA7 (the latter is the blue pill on-board LED).
 *
 * The bits on PB8..PB15 will be slightly wild: Each bit-period is split into
 * twol halves; during the first, the output is driven as on PA0..PA7, but
 * during the second half of the value for each bit, that bit will float.
 */
void tsr_count (TimerHandle_t self) {
	//
	// Increment the counter
	counter++;
	//
	// For each bit in the counter, whether it is in its first or
	// second half-life depends on the bit below it.  Since the low
	// bit of the counter is not shown externally, this can be
	// determined for all externally visible bits.
	// In the second half-life, the bits on PB8..PB15 will float, so
	// they need to switch, while they are driven as outputs in their
	// first half.  To facilitate this, derive a bit masks to decide
	// what bits are floating; the others will be driven.
	uint32_t mask_float = 0;
	uint8_t halflives = counter & 0xff;
	for (int bitnr = 0; bitnr <= 7; bitnr++) {
		//
		// Proceed to the next 4 bits of GPIOx_CR2 masking
		mask_float >>= 4;
		//
		// Check for each bit if it is in its second-half
		if ((halflives & 0x01) == 0x01) {
			mask_float |= 0xf0000000;
		}
		//
		// Proceed to the next bit
		halflives >>= 1;
	}
	//
	// Export the 8-bit counter value after stripping the
	// least-valued bit (which serves to detect half-time)
	GPIOA_ODR = (counter >> 1) & 0x00ff;
	GPIOB_ODR = (counter << 7) & 0xff00;
	BIT (GPIOC_ODR,13) = ((counter & 0x0100) != 0x000);
	//
	// Float PBx as input that are in their second half-life,
	// drive PBx-outputs that are in their first half-life
	// and deliberately do this after flipping any bits and
	// aware of non-change when a bit enters second half-life
	if ((counter & 0x01) == 0x00) {
		GPIOB_CRH = (0x44444444 & mask_float) | (0x88888888 & ~mask_float);
	}
}


static TimerHandle_t counter_timer;


void main (void) {
	//
	// Setup the hardware drivers
	setupDriver ();
	//
	// Replace the GPIOA modes for A0..A7 to output
	GPIOA_CRL = 0x88888888;
	//
	// Replace the GPIOB modes for B8..B15 to output
	GPIOB_CRH = 0x88888888;
	//
	// Create a timer task; the period is such that the PA7/PB15 toggle
	// once per second, so PA0/PB8 toggle 256x second and the bit below,
	// which is the lowest bit of the counter, toggles 512x per second.
	// The latter is how often the counter-increment timer task runs.
	counter_timer = xTimerCreate (
			"counter_timer",
			pdMS_TO_TICKS(1),
			pdTRUE,
			&counter,
			tsr_count);
	assert (counter_timer != NULL);
	//
	// Start the created timer
	assert (pdPASS == xTimerStart (counter_timer, 0));
	//
	// Start activity for the tasks (which is just the counter_timer)
	vTaskStartScheduler ();
	assert (false);		/* Only end here when scheduling failed */
}
