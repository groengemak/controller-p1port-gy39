/* Test for bit-banding on ARM Cortex-M3.
 *
 * This code is part of <STM32F/ports.h> and should be tested.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <STM32F/ports.h>


int main (int argc, char *argv []) {
	if (argc < 2) {
		fprintf (stderr, "Usage: %s ADDRESS BITNR -or- %s ADDRESS...\nWhere the first form translates address/bits to a bit-band address\nand the second form translates 1 or 3+ bit-band addresses back to register/SRAM address and bit number\n", argv[0], argv[0]);
	} else if (argc == 3) {
		char *endptr = NULL;
		uint32_t *const address = (uint32_t *) strtoul (argv [1], &endptr, 0);
		if ((endptr == NULL) || (*endptr != '\0')) {
			fprintf (stderr, "Mal-formed address %s\n", argv [1]);
			exit (1);
		}
		if ((((intptr_t) address) & 0x0e000000) != 0x00000000) {
			fprintf (stderr, "Not a base-range address %s\n", argv [1]);
			exit (1);
		}
		int bitnr = atoi (argv [2]);
		if ((bitnr < 0) || (bitnr > 31)) {
			fprintf (stderr, "Illegal bit number %s\n", argv [2]);
			exit (1);
		}
		//
		// Print a few forward-computation tests
		uint32_t *const banded  = &BIT (*address,bitnr);
#ifdef DEBUG
		uint32_t *const banded0 = &BIT0(*address,bitnr);
		uint32_t *const banded1 = &BIT1(*address,bitnr);
		uint32_t *const banded2 = &BIT2(*address,bitnr);
		uint32_t *const banded3 = &BIT3(*address,bitnr);
#endif
		printf ("Address 0x%08lx bit%3d bit-bands on 0x%08lx\n",
				(uint32_t) (intptr_t) address, bitnr, (uint32_t) (intptr_t) &BIT(*address,bitnr));
#ifdef DEBUG
		printf ("Address 0x%08lx bit%3d bit-bands on 0x%08lx\n",
				(uint32_t) (intptr_t) address, bitnr, (uint32_t) (intptr_t) banded );
		printf ("Address 0x%08lx bit%3d bit-band0 on 0x%08lx\n",
				(uint32_t) (intptr_t) address, bitnr, (uint32_t) (intptr_t) banded0);
		printf ("Address 0x%08lx bit%3d bit-band1 on 0x%08lx\n",
				(uint32_t) (intptr_t) address, bitnr, (uint32_t) (intptr_t) banded1);
		printf ("Address 0x%08lx bit%3d bit-band2 on 0x%08lx\n",
				(uint32_t) (intptr_t) address, bitnr, (uint32_t) (intptr_t) banded2);
		printf ("Address 0x%08lx bit%3d bit-band3 on 0x%08lx\n",
				(uint32_t) (intptr_t) address, bitnr, (uint32_t) (intptr_t) banded3);
		printf ("Address 0x%08lx with &* prefix it's 0x%08lx\n",
				(uint32_t) (intptr_t) address, (uint32_t) (intptr_t) &*address);
#endif
	} else {
		//
		// Map addresses to bit-banding addresses
		for (int argi = 1; argi < argc; argi++) {
			char *endptr = NULL;
			uint32_t address = strtoul (argv [argi], &endptr, 0);
			if ((endptr == NULL) || (*endptr != '\0')) {
				fprintf (stderr, "Mal-formed address %s\n", argv [argi]);
				exit (1);
			}
			if ((address & 0x0e000000) != 0x02000000) {
				fprintf (stderr, "Not a bit-band addres %s\n", argv [argi]);
				exit (1);
			}
			uint32_t real8  = (address & 0x60000000) | ((address & 0x01ffffe0) >> 5);
			uint32_t real32 = (address & 0x60000000) | ((address & 0x01ffff80) >> 5);
			int bit8  = (address >> 2) & 0x07;
			int bit32 = (address >> 2) & 0x1f;
			printf ("Address 0x%08lx addresses bit %d in 0x%08lx or bit%3d in 0x%08lx\n",
				address, bit8, real8, bit32, real32);
		}
	}
}
