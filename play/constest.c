/* constest.c -- Detect a high level or rising edge on GPI,CONS
 *
 * Measure the current level once in a few seconds and update the BoardLED;
 * while waiting between polls, check for a rising edge and show that as an
 * independently timed short inversion (looking like flash on or flash off).
 *
 * So, set the level and wait for slow responses.  Except that a rising edge
 * will immediate make the LED flash (and possibly pickup on the level later).
 * Edges' flashes end by switching off the BoardLED, so a small dip would even
 * be shown; it would switch off the BoardLED until the next regular check.
 *
 * When the console is detected, start sending "Hello Hot World\r\n" messages.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <driver.h>


const char msg [] = "Hello Hot World\r\n";

const int msglen = sizeof (msg) - 1;

void main (void) {
	setupDriver ();
	for (;;) {
		//
		// Copy the BoardLED from the current CONS signal
		bool slowstate = getCONS ();
		boardLED (slowstate);
		//
		// Send the message
		//TODO// serialTX (3, msg, msglen);
		//
		// Now loop and sample
		int off = -99;
		for (int on = 999999; on > 0; on--) {
			// ISR_EXTI_9_5 ();
			if (on == off) {
				boardLED (slowstate);
			} else if (risenCONS ()) {
				boardLED (!slowstate);
				off = on - 25;
			}
		}
#if 0
		int off = -99;
		for (int on = 999999; on > 0; on--) {
			if (on == off) {
				boardLED (slowstate);
			} else if (risenCONS ()) {
				boardLED (!slowstate);
				off = on - 25;
			}
		}
#endif
	}
}
