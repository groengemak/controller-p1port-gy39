;@ Booting code for STM32F103C8T6 microcontrollers
;@
;@ When booting from user flash, this area is mapped to address 0x00000000
;@ where it fills the NVIC vector.  This includes an address for the stack
;@ pointer and a reset pointer, among others.
;@
;@ Code uses addresses/sizes specific to STM32F103C8T6 and was inspired on
;@ https://github.com/dwelch67/stm32_samples/blob/master/STM32F103C8T6/blinker01/flash.s
;@
;@ From: Rick van Rein <rick@openfortress.nl>


	.cpu	cortex-m3
	.thumb

;@
;@ First flash contents: NVIC table: initSP, initPC, ISR...
;@
	.thumb_func
	.global	_start
_start:
stacktop:
	.word	0x20000000 + 20480
	.word	reset
	.word	hang
	.word	hang
	.word	hang
	.word	hang
	.word	hang
	.word	hang
	.word	hang
	.word	hang
	.word	hang
	.word	hang
	.word	hang
	.word	hang
	.word	hang
	.word	hang

	.thumb_func
reset:	bl	notmain
	b	hang

;@
;@ Hang in an infinite loop
;@
	.thumb_func
hang:	b	.
