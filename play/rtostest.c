/* rtostest.c -- Minimal blinking-LED test to gain control with FreeRTOS.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdbool.h>

#include <driver.h>

#include <FreeRTOS.h>
#include <task.h>


StaticTask_t statblink;
StackType_t blinkstack [64];
void blinktask (void *parm) {
	while (1) {
		boardLED (true);
		for (int i = 0; i < 999999; i++) { nopCall (); }
		boardLED (false);
		for (int i = 0; i < 999999; i++) { nopCall (); }
	}
}

void main (void) {
	setupDriver ();
#if 0
	TaskHandle_t blink = xTaskCreateStatic (
			blinktask,
			"blinktask",
			sizeof (blinkstack) / sizeof (StackType_t),
			(void *) 0,
			1,
			blinkstack,
			&statblink);
#else
	TaskHandle_t blink;
	BaseType_t rv = xTaskCreate (
			blinktask,
			"blinktask",
			sizeof (blinkstack) / sizeof (StackType_t),
			(void *) 0,
			1,
			&blink);
	assert (rv == pdPASS);
#endif
	vTaskStartScheduler ();
	assert (false);		/* Only end here when scheduling failed */
}
