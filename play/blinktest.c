/* blinktest.c -- Minimal blinking-LED test to gain control.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdbool.h>

#include <driver.h>


void main (void) {
	setupDriver ();
	while (1) {
		boardLED (true);
		for (int i = 0; i < 999999; i++) { nopCall (); }
		boardLED (false);
		for (int i = 0; i < 999999; i++) { nopCall (); }
	}
}
