/* Play with modebits for GPIOx_CR[HL] on STM32F.
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


const char *mode [4] = {
	"input (as reset)",
	"output <= 10 MHz",
	"output <=  2 MHz",
	"output <= 50 MHz"
};

const char *config_in [4] = {
	"analog input signal ",
	"floating  (as reset)",
	"input pulled up|down",
	"RESERVED            "
};

const char *config_out [4] = {
	"GPIO line push-pull ",
	"GPIO line open-drain",
	"AFIO line push-pull ",
	"AFIO line open-drain"
};


int main (int argc, char *argv []) {
	int retval = 0;
	if (argc < 2) {
		fprintf (stderr, "Usage: %s 0x12345678...\n", *argv);
		exit (1);
	}
	for (int argi = 1; argi < argc; argi++) {
		char *endptr = NULL;
		unsigned long crHL = strtoul (argv [argi], &endptr, 0);
		if ((crHL > 0xffffffff) || (endptr == NULL) || (*endptr != '\0')) {
			fprintf (stderr, "Invalid number: %s\n", argv [argi]);
			retval = 1;
		} else {
			for (int dignr = 7; dignr >= 0; dignr --) {
				int digit = (crHL >> (4 * dignr)) & 0x0000000f;
				printf ("%s H=%2d|L=%2d digit %1x or %d%d%d%d is mode %s, config %s\n",
					argv [argi],
					dignr+8, dignr+0,
					
					digit,
					(digit >> 3) & 1, (digit >> 2) & 1, (digit >> 1) & 1, digit & 1,
					mode [digit & 0x03],
					(((digit & 0x03) == 0x00) ? config_in : config_out) [digit >> 2]);
			}
		}
	}
	exit (retval);
}
