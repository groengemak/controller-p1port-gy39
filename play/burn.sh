#!/bin/sh
#
# Flash the current binary over the ST-Link-V2

BINFILE=blinktest.bin
# BINFILE=constest.bin
# BINFILE=rtostest.bin
# BINFILE=p1test.bin

if [ ../src/driver-STM32F.c -nt $BINFILE ]
then
	echo >&2 "ERROR: Driver is newer than binary in $BINFILE"
	exit 1
else
	st-flash write "$BINFILE" 0x08000000
	if [ $? -ne 0 ]
	then
		echo >&2 "ERROR: Flashing failed"
		exit 1
	else
		echo >&2 "SUCCESS."
		exit 0
	fi
fi
