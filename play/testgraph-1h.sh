#!/bin/sh
#
# Draw a graph over the last hour, args [[RRDfile] PNGfile]
#
# From: Rick van Rein <rick@openfortress.nl>


if [ $# -eq 0 ]
then
	RRDFILE="/tmp/test.rrd"
	PNGFILE="/tmp/test.png"
elif [ $# -eq 1 ]
then
	RRDFILE="/tmp/test.rrd"
	PNGFILE="$1"
elif [ $# -eq 2 ]
then
	RRDFILE="$1"
	PNGFILE="$2"
else
	echo >&1 "Usage: $0 [[file.rrd] file.png]"
	exit 1
fi

rrdtool graph "$PNGFILE" --end now --start now-3600s --width 400 "DEF:t=$RRDFILE:temp:AVERAGE" "DEF:h=$RRDFILE:humid:AVERAGE" 'LINE1:t#0000ff:Temperature [⁹C]\l' 'LINE1:h#00ff00:Humidity [%]\l'

