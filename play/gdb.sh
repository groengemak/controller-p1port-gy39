#!/bin/sh
#
# Start GDB while loading the current program.

SYMBOLFILE=blinktest
# SYMBOLFILE=constest
# SYMBOLFILE=rtostest
# SYMBOLFILE=p1test

setsid st-util &

gdb-multiarch \
	-ex "symbol-file $SYMBOLFILE" \
	-ex 'target remote :4242' \
	-ex 'layout asm'

killall st-util
