/* Console output.
 *
 * Send out short notices about important events.  Or not, if nobody is listening.
 *
 * From: Rick van Rein <rick@groengemak.nl>
 */


#include <stdbool.h>

#include <FreeRTOS.h>
#include <task.h>

#include <driver.h>
#include <console.h>



static TaskHandle_t constask;

const char fullmsg [] = "Hello Hot World\r\n";

struct consput fullmsgput = {
	.inuse = false,
	.outdated = false,
	.buf = fullmsg,
	.buflen = sizeof (fullmsg)-1,
};


#if 0
/* Interrupt Service Routine for CONS output completion on CONS.
 * Note: Not called for an empty output buffer, but when actually done.
 */
void isr_cons_txd_dma (void) {
	//
	// Toggle the board LED
	static bool blinker = false;
	blinker = !blinker;
	boardLED (blinker);
	//
	// Restart the whole string sending procedure
	send_CONS (fullmsg, fullmsglen);
}
#endif

#include <STM32F/ports.h>
volatile int debug_q;
volatile int *debug_p = &debug_q;
void debugnow (void) {
	int usart2sr = USART2_SR;
	int dma1len = DMA1_CNDTR7;
	int dma1isr = DMA1_ISR;
	REGISTER_BARRIER ();
	*debug_p = usart2sr + dma1len + dma1isr;
}

/* Transmit characters over the CONS serial output in an infinite loop.
 * Since this is done in the background, end by yielding to others.
 */
void sendmsg_console (void *param) {
#if 1
	bool bled = false;
	while (true) {
		for (int i = 0; i < 1000000; i++) {
			nopCall ();
		}
		if (!fullmsgput.inuse) {
			bled = !bled;
			boardLED (bled);
			consput (&fullmsgput);
		}
	}
#endif
	//
	// Loop forever, yielding to other tasks
#if 0
bool led = false;
#endif
	while (true) {
		taskYIELD ();
		debugnow ();
#if 0
		for (int i = 0; i < 1000; i++) {
			nopCall ();
		}
		boardLED (led);
		led = !led;
		start_send_CONS ();
		send_CONS (fullmsg, fullmsglen);
		for (int i = 0; i < 10000; i++) {
			taskYIELD ();
		}
		stop_send_CONS ();
#endif
	}
}


/* Main program for infinite CONS message transmission.
 */
void main (void) {
	//
	// Initialise the program's tasks
	setupDriver ();
	setupConsole ();
	//
	// Start the console task over CONS
	// (In fact a placeholder doing nothing...)
	BaseType_t rv = xTaskCreate (
		sendmsg_console,
		"console",
		64,
		(void *) 0,
		1,
		&constask);
	assert (rv = pdPASS);
	//
	// Hand over control to FreeRTOS
	// (In fact a contrived "csie i" instruction)
	vTaskStartScheduler ();
	//
	// Control should not get here
	assert (false);
}
