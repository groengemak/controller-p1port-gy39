# Modbus code for P1 Port and GY-39

> *This code can be flashed into a microcontroller.
> It listens to the Open Metering Standard
> as well as GY-39 wheather sensors.
> The data is made available as a Modbus service*

**Inputs:**

  * P1 Port (the Dutch name is "slimme meter") similar/equal to the Open Metering Standard
  * GY-39 sensors for wheather (temperature, humidity, air pressure, lightness); support 0x, 1x, 2x or a bus of up to 16x

**Outputs:**

  * A modbus connector over RS-485, after a converter from RS-232 at 3V3 level
  * A console connector as an RS-232 connector at 3V3 level (TxD only, RxD stop signal is used to detect the listener)

**Hardware:**

  * A microcontroller such as STM32F103C8T6 (the "blue pill" standard board would do) with a few resistors, capacitors, transistors
      - Both USART1 pinouts are used to represent two GY-39 sensors (more when using a bus)
      - A possible design alternative, but not currently planned, could be an ESP32, so as to support WiFi and Bluetooth
  * An RJ11 or RJ12 connector for the P1 Port; 6P4C is RJ11 and can signal; 6P6C is RJ12 and can tap 5V from the P1 Port
  * Any number of GY-39 modules (connected over RS-232 to allow for some distance, convert to symmetric signals where necessary)
  * Optional CD4066 switch modules to support a bus (with more than 2x GY-39 sensors)
  * A conversion module between RS-232 at "TTL level" and RS-485 to carry Modbus signals (a long-distance bus)
  * A 2.5mm stereo audio jack (board) connector for the ARPA2 standard console plug

**Software:**

  * FreeRTOS to allow flexible multitasking
  * Ragel is used to generate a very small parser to collect data from P1 Port bursts (current size < 1kB)
  * The setup is prepared for the STM32F architecture, specifically the popular STM32F103C8T6
  * A port of gcc and binutils for the desired architecture will be used (and gdb-multiarch is useful)
  * Several scripts rely on st-* utilities (st-flash, st-info)

**Modbus:**

  * Measurements are initiated by Modbus
      - The P1 Port is not asserted continually, but only when requested
      - The GY-39 sensors are not polled continually, but only when requested
  * There are registers for the various P1 Port outputs
      - Concrete P1 Ports may not deliver contiguous data
      - Non-answerable register values will be trivially filled (often 0)
  * There are registers for the various GY-39 measurements
      - This forms a contiguous array of GY-39 sensors
      - Each of the GY-39 sensors presents multiple measurements
      - Addresses for GY-39 sensors depend on SELn bits with even/odd sides
      - The sensor addresses are used to determine controller driving choices
      - In other words, the Modbus queries are for a hardware alternative configuration

**Building:**

  * You need to select a platform, like STM32F, with support in this repository
  * You need to run CMake once with the device plugged in, to detect its memory sizes
